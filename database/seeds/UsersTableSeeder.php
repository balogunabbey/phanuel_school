<?php

use Illuminate\Database\Seeder;
use App\User;
// use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//truncate user table
        // User::truncate();

        //create the user roles
        // $webmasterRole = Role::where('name', 'webmaster')->first();
        // $propietorRole = Role::where('name', 'propietor')->first();
        // $headmasterRole = Role::where('name', 'headmaster')->first();
        // $teacherRole = Role::where('name', 'teacher')->first();
        // $parentRole = Role::where('name', 'parent')->first();


         //create the user infos
        $webmaster = User::create([
        	'name' => 'Balogun Abiodun',
        	'email' => 'webmaster@phanuelschools.sch.ng',
        	'isAdmin' => '1',
        	'password' => bcrypt('value')

        ]);


        $propietor = User::create([
        	'name' => 'Dewunmi Rabio',
        	'email' => 'propietor@phanuelschools.sch.ng',
        	'isAdmin' => '1',
        	'password' => bcrypt('value')

        ]);


        $headmaster = User::create([
        	'name' => 'Adijat Adebogun',
        	'email' => 'headmaster@phanuelschools.sch.ng',
        	'isAdmin' => '1',
        	'password' => bcrypt('headmaster')

        ]);



        $teacher = User::create([
        	'name' => 'Fatima Adebogun',
        	'email' => 'fatima@phanuelschools.sch.ng',
        	'isAdmin' => '1',
        	'password' => bcrypt('teacher')

        ]);


        $parent = User::create([
        	'name' => 'Yusirat Balogun',
        	'email' => 'ucyworld@yahoo.com',
        	'isAdmin' => '0',
        	'password' => bcrypt('parent')

        ]);


         //attach roles to users
        // $webmaster->roles()->attach($webmasterRole);
        // $propietor->roles()->attach($propietorRole);
        // $headmaster->roles()->attach($headmasterRole);
        // $teacher->roles()->attach($teacherRole);
        // $parent->roles()->attach($parentRole);



    }
}
