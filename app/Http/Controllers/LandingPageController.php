<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackRequest;
use App\Http\Requests\ShareTestimonyRequest;
use App\Http\Requests\EnrolRequest;
use Illuminate\Http\Request;

Use App\Feedback;
Use App\Testimony;
use App\Enroll;


class LandingPageController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    return view('welcome');
    }

    public function classes()
    {
    return view('rooms');
    }

    public function basic()
    {
    return view('basic');
    }

    public function creche()
    {
    return view('creche');
    }

    public function nursery()
    {
    return view('nursery');
    }

    public function about()
    {
    return view('about');
    }

    public function staffs()
    {
    return view('staffs');
    }

    public function pta()
    {
    return view('pta');
    }

    public function vacancy()
    {
    return view('vacancy');
    }

    public function admission()
    {
    return view('admission');
    }

    public function enrolForm()
    {
    return view('enrol');
    }

    public function enrolNow(EnrolRequest $request)
    {
        //validate
        $validated = $request->validated();

        $enrol = new Enroll;
        $enrol->first_name = $request->first_name;
        $enrol->last_name = $request->last_name;
        $enrol->dob = $request->dob;
        $enrol->gender = $request->gender;
        $enrol->blood_group = $request->blood_group;
        $enrol->prev_school_name = $request->prev_school_name;
        $enrol->prev_class = $request->prev_class;
        $enrol->intend_class = $request->intend_class;
        $enrol->parent_first_name = $request->parent_first_name;
        $enrol->parent_last_name = $request->parent_last_name;
        $enrol->email_address = $request->email_address;
        $enrol->phone_number = $request->phone_number;
        $enrol->occupation = $request->occupation;
        $enrol->home_address = $request->home_address;
        $enrol->religion = $request->religion;
        $enrol->allergy = $request->allergy;

        if($enrol->save()){

        $notification=[
               'message'=>'Your application was sent successfully',
               'alert-type'=>'success'
           ];

       return redirect()->route('admission.enrol')->with($notification);
       }
       else{

       $notification=[
           'message'=>'something went wrong, try again or contact webmaster',
           'alert-type'=>'error'

       ];
       return redirect()->back()->route('admission.enrol')->with($notification);
       }
    }


    public function curriculum()
    {
    return view('curriculum');
    }

    public function kidszone()
    {
    return view('kidszone');
    }

    public function termDates()
    {
    return view('termsdate');
    }

    public function internetSafety()
    {
    return view('internetsafety');
    }

    public function healthCare()
    {
    return view('healthcare');
    }


    public function absences()
    {
    return view('absences');
    }

    public function testimony()
    {
    return view('testimony');
    }

    public function shareTestimony(ShareTestimonyRequest $request)
    {
            //validate
        $validated = $request->validated();

        $testify = new Testimony;
        $testify->fullname = $request->full_name;
        $testify->email = $request->your_email;
        $testify->phonenumber = $request->your_phone;
        $testify->opinion = $request->your_opinion;


        if($testify->save()){

             $notification=[
                'message'=>'Your Message was sent successfully',
                'alert-type'=>'success'
            ];

        return redirect()->route('testimony.page')->with($notification);
        }
        else{

        $notification=[
            'message'=>'something went wrong, try again or contact webmaster',
            'alert-type'=>'error'

        ];
        return redirect()->back()->route('testimony.page')->with($notification);
        }

    }

    public function news()
    {
     return view('news');   
    }

    public function newsPreview()
    {
     return view('newspreview');   
    }

    public function gallery()
    {
     return view('gallery');   
    }

    public function galleryDisplay()
    {
     return view('gallerydisplay');   
    }

    public function contactus()
    {
    return view('contact');
    }

    public function feedback(FeedbackRequest $request)
    {

        $validated = $request->validated();

        $feedback = new Feedback;
        $feedback->name = $request->your_name;
        $feedback->email = $request->your_email;
        $feedback->phonenumber = $request->your_phone;
        $feedback->enquiry = $request->your_message;

         // $feedback =  Feedback::create($request->all());


        if($feedback->save()){

             $notification=[

                'message'=>'Your Message was sent successfully',
                'alert-type'=>'success'

            ];

        return redirect()->route('contactus.page')->with($notification);
        }
        else{

        $notification=[

            'message'=>'something went wrong, try again or contact webmaster',
            'alert-type'=>'error'

        ];
        return redirect()->back()->route('contactus.page')->with($notification);
        // news.page
        }

    }

    public function privacy()
    {
     return view('privacy');   
    }





}
