<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EnrolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:50',
            'last_name' => 'required|string|max:50',
            'dob' => 'required',
            'gender' => 'required',
            'blood_group' => 'required',
            'allergy' => 'sometimes|string',
            'prev_school_name' => 'required|string|max:100',
            'prev_class' => 'required|string|max:40',
            'intend_class' => 'required|string|max:40',
            'parent_first_name' => 'required|string|max:50',
            'parent_last_name' => 'required|string|max:50',
            'email_address' => 'required|string|email',
            'phone_number'=> 'required|digits:11',
            'occupation' => 'required',
            'home_address' => 'required',
            'religion' => 'required:string'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.required' => 'Kindly provide first name!',
             'last_name.required' => 'Kindly provide last name!',
            'dob.required' => 'Kindly provide date of birth!',
            'gender.required' => 'Select preferred gender!',
            'blood_group.required' => 'Select preferred blood group!',
            'prev_school_name.required' => 'Kindly provide previous school!',
            'prev_class.required' => 'Kindly provide previous class!',
            'intend_class.required' => 'Kindly provide intending class!',
            'parent_first_name.required' => 'Kindly provide parent first name!',
            'parent_last_name.required' => 'Kindly provide parent last name!',
            'email_address.required' => 'Kindly provide your email address!',
            'phone_number.required' => 'Kindly provide your phone number!',
            'occupation.required' => 'Kindly provide parent occupation!',
            'home_address.required' => 'Kindly provide parent home address',
            'religion.required' => 'Select preferred religion',

            //other validations
            'first_name.max' => 'Should no exceed 50 characters!',
            'last_name.max' => 'Should no exceed 50 characters!',
            'prev_school_name.max' => 'Should no exceed 100 characters!',
            'prev_class.max' => 'Should no exceed 40 characters!',
            'intend_class.max' => 'Should no exceed 40 characters!',
            'parent_first_name.max' => 'Should no exceed 50 characters!',
            'parent_last_name.max' => 'Should no exceed 50 characters!',
            'email_address.email' => 'Kindly provide a valid email address!',
            'phone_number.digits' => 'Kindly provide a valid phone number!'
        ];
    }
}
