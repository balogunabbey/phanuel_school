<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShareTestimonyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|string|max:100',
            'your_email' => 'required|string|email|max:255',
            'your_phone'=> 'required|digits:11',
            'your_opinion' => 'required|max:500'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
           'full_name.required' => 'Kindly provide your name in full!',
            'your_email.required' => 'Kindly provide your email address!',
            'your_phone.required' => 'Kindly provide your phone number!',
            'your_opinion.required' => 'Kindly share your experience with us via the textbox!',

            //other validations
            'full_name.max' => 'Should no exceed 50 characted words!',
            'your_email.email' => 'Kindly provide a valid email address!',
            'your_phone.integer' => 'Kindly provide a valid phone number!',
            'your_opinion.max' => 'Should no exceed 500 characted words!'
        ];
    }
}
