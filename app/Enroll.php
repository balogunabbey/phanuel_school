<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enroll extends Model
{
    protected $fillable = [ 'first_name', 'last_name', 'dob', 'gender', 'blood_group', 'allergy', 'prev_school_name', 'prev_class', 'intend_class', 'parent_first_name', 'parent_last_name', 'email_address', 'phone_number', 'occupation', 'home_address', 'religion'
    ];

}
