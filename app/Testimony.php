<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimony extends Model
{

protected $fillable = ['fullname', 'email', 'phonenumber','opinion'];

}
