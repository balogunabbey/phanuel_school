<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');

Auth::routes();

/**
* All Web Routes.
*/
//Artisan Command Line
//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

//Welcome Page
Route::get('/','LandingPageController@index')->name('home.page');

//Discover
Route::get('school/about-us','LandingPageController@about')->name('about.page');
Route::get('school/parents-teachers-association','LandingPageController@pta')->name('pta.page');
Route::get('school/vacancy','LandingPageController@vacancy')->name('vacancy.page');


//Admissions
Route::get('admission/procedure','LandingPageController@admission')->name('admission.page');
Route::get('admission/enrol','LandingPageController@enrolForm')->name('admission.enrol');
Route::post('admission/enrol/submit','LandingPageController@enrolNow')->name('enrol.submit');



//Classes
Route::get('sessions/classes','LandingPageController@classes')->name('classes.page');
Route::get('classes/basic-education','LandingPageController@basic')->name('basic.page');
Route::get('classes/nursery','LandingPageController@nursery')->name('nursery.page');
Route::get('classes/creche','LandingPageController@creche')->name('creche.page');


//Pupils
Route::get('pupils/curriculum','LandingPageController@curriculum')->name('curriculum.page');
Route::get('pupils/kidszone','LandingPageController@kidszone')->name('kidszone.page');


//Teachers
Route::get('teachers','LandingPageController@staffs')->name('staffs.page');


//Parents
Route::get('parent/term-dates','LandingPageController@termDates')->name('termsDate.page');
Route::get('parent/internet-safety','LandingPageController@internetSafety')->name('internetSafety.page');
Route::get('parent/health-care','LandingPageController@healthCare')->name('healthCare.page');
Route::get('parent/absences','LandingPageController@absences')->name('absences.page');
Route::get('parent/share-your-story','LandingPageController@testimony')->name('testimony.page');
Route::post('parent/share/testimony','LandingPageController@shareTestimony')->name('share.testimony');


//Media
Route::get('media/gallery','LandingPageController@gallery')->name('gallery.page');
Route::get('media/gallery/display','LandingPageController@galleryDisplay')->name('gallerydisplay.page');
Route::get('media/news-events','LandingPageController@news')->name('news.page');
Route::get('media/news-events/newspreview','LandingPageController@newsPreview')->name('newspreview.page');


//Contact US
Route::get('school/contact-us','LandingPageController@contactus')->name('contactus.page');
Route::post('school/contact/feedback','LandingPageController@feedback')->name('feedback.page');

//Privacy Policy
Route::get('privacy-policy','LandingPageController@privacy')->name('privacy.page');







