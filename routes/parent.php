<?php
Route::group(['prefix' => 'parent', 'namespace' => 'Parent'], function() {

	     Route::get('dashboard','DashboardController@dashboard')->name('dashboard.index');

	     //feedback/Suggestion
	     Route::get('tickets','DashboardController@suggestion')->name('open.ticket');

	     //notifications
	     Route::get('announcement','DashboardController@announcement')->name('notice.index');

	     //Absence Permission
	     Route::get('absence/request','DashboardController@absence')->name('absence.index');


	     //Pickup Assistant
	     Route::get('pickup-assistant','DashboardController@pickup')->name('pickup.index');
		 Route::get('pickup-assistant/create','DashboardController@addPickup')->name('pickup.new');
		 
		//Kids
		Route::get('registered/allkids','DashboardController@kids')->name('kids.index');
		Route::get('school/enrol','DashboardController@enrol')->name('enrol.new');

		//Payments
		Route::get('tuition/payments','DashboardController@tuition')->name('tuition.index');

		//Result Checker
		Route::get('grades/checker','DashboardController@resultChecker')->name('grade.index');
		Route::get('results/summary/slug','DashboardController@resultSummary')->name('grade.summary');

		//Setting
		Route::get('general/settings','DashboardController@allSettings')->name('setting.index');








});