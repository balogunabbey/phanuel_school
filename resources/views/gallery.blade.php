@extends('layouts.app')
@section('title', 'Gallery')
@section('content')

<!-- Inner Banner -->
<div class="inner-banner gallery text-center" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-03.jpg') }}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>Phanuel Schools Gallery</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

    <!-- Gallery Holder -->
    <section class="gallery-holder tc-padding-bottom gray-bg">
        <div class="container">
            <div class="content has-layout">

                <!-- Breadcrumbs -->
                <div class="breadcrumbs">
                    <ul>
                        <li><i class="icon-folder"></i> Gallery</li>
                        <li><a href="{{ route('gallery.page') }}"><i class="icon-home22"></i> Back to Home</a></li>
                    </ul>
                </div>
                <!-- Breadcrumbs -->

                <!-- Gallery -->
                <div class="gallery">

                    <!-- Main Heading -->
                    <div class="main-heading-holder">
                        <div class="main-heading">
                            <p>&nbsp;</p>
                            <h2>Our Photo Gallery</h2>
                        </div>
                    </div>
                    <!-- Main Heading -->

                    <!-- Gallery -->
                    <div class="row">



                        <!-- gallery Figure -->
                        <div class="col-sm-3 col-sm-4 col-xs-6 r-full-width wow fadeInUp">
                            <figure class="gallery-figure rotate-2">
                                <img src="{{ asset('static/images/3.jpg') }}" alt="Carol Nights 2017">
                                <figcaption class="overlay">
                                    <h4 class="position-center-center"><a href="{{ route('gallerydisplay.page')}}">Carol
                                            Nights 2017</a></h4>
                                </figcaption>
                            </figure>
                        </div>
                        <!-- gallery Figure -->

                        <!-- gallery Figure -->
                        <div class="col-sm-3 col-sm-4 col-xs-6 r-full-width wow fadeInUp">
                            <figure class="gallery-figure rotate-1">
                                <img src="{{ asset('static/images/2.jpg') }}" alt="">
                                <figcaption class="overlay">
                                    <h4 class="position-center-center"><a
                                            href="{{ route('gallerydisplay.page')}}">Christmas Term 2017</a></h4>
                                </figcaption>
                            </figure>
                        </div>
                        <!-- gallery Figure -->

                    </div>
                    <!-- End Row -->
                </div>
                <!-- Gallery -->

            </div>
        </div>
    </section>
    <!-- Gallery Holder -->

</main>
<!-- Main -->
@endsection