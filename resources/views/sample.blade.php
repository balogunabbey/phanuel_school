@extends('layouts.app')
@section('title', 'Welcome')
@section('content')
<!-- Banner Slider -->
<div id="banner-slider" class="banner-slider curve-up">
    <img class="sp-image" src="{{asset('static/images/slider/bg.jpg')}}" alt="">

    <!-- Slides -->
    <div class="sp-slides">

        <!-- Slide 1 -->
        <div class="sp-slide">
            <!-- Caption -->
            <div class="sp-layer sp-padding caption-text" data-horizontal="271" data-vertical="40"
                data-show-transition="left" data-hide-transition="left" data-show-delay="600" data-hide-delay="700">
                <div class="banner-caption">
                    <span>Welcome to</span>
                    <h1>PHANUEL SCHOOLS</h1>
                    <p>Here is an exciting, motivating and inclusive school<br> where every individual, old and young,
                        is valued.<br> We believe that education is a partnership between home and<br> school and we try
                        to involve parents in all aspects of school life.</p>
                    <a class="tc-btn" href="">Discover More</a>
                </div>
            </div>
            <!-- Caption -->
            <!-- Banner Layers -->
            {{-- <img class="sp-layer layer-img-1" data-horizontal="919" data-vertical="-5" data-show-transition="right" data-hide-transition="up" data-show-delay="400" data-hide-delay="200" src="{{ asset('static/images/slider/.png') }}"
            alt=""> --}}
            <img class="sp-layer layer-img-2" data-horizontal="750" data-vertical="-5" data-show-transition="up"
                data-hide-transition="up" data-show-delay="800" data-hide-delay="500"
                src="{{ asset('static/images/slider/slider-layer-2.png') }}" alt="">
            <!-- Banner Layers -->
        </div>
        <!-- Slide 1 -->

        <!-- Slide 1 -->
        <div class="sp-slide">
            <!-- Caption -->
            <div class="sp-layer sp-padding caption-text" data-horizontal="271" data-vertical="40"
                data-show-transition="left" data-hide-transition="left" data-show-delay="600" data-hide-delay="700">
                <div class="banner-caption">
                    <span>At</span>
                    <h1>PHANUEL SCHOOLS</h1>
                    <p>We aim to create an environment where each child is made to <br><span>feel secure, wanted and
                            valued. we make every<br> effort to produce a happy atmosphere with mutual respect<br> shown
                            by everyone in our school family.</span></p>
                    <a class="tc-btn" href="../admissions">Get Enroll Now</a>
                </div>
            </div>
            <!-- Caption -->
            <!-- Banner Layers -->
            {{-- <img class="sp-layer layer-img-1" data-horizontal="919" data-vertical="-5" data-show-transition="right" data-hide-transition="up" data-show-delay="400" data-hide-delay="200" src="{{ asset('static/images/slider/img-01.png') }}"
            alt=""> --}}
            <img class="sp-layer layer-img-2" data-horizontal="750" data-vertical="-5" data-show-transition="up"
                data-hide-transition="up" data-show-delay="800" data-hide-delay="500"
                src="{{ asset('static/images/slider/slider-layer-1.png') }}" alt="">
            <!-- Banner Layers -->
        </div>
        <!-- Slide 1 -->



    </div>
    <!-- Slides -->

    <!-- Paginaition dots -->
    <ul class="sp-thumbnails">
        <li class="sp-thumbnail"></li>
        <li class="sp-thumbnail"></li>
    </ul>
    <!-- Paginaition dots -->

</div>
<!-- Banner Slider -->

<!-- Main -->
<main id="main">

    <!-- Services -->
    <section class="services-holder">
        <div class="container">
            <div class="p-relative has-layout">

                <!-- Alert -->
                <div class="alert-1">
                    <p style="color: #ea6f83;"><i class="icon-star-full"></i>Register your child today at PHANUEL
                        SCHOOLS, a great place to learn through play.</p>
                    <a class="tc-btn" href="">Get More Info</a>
                </div>
                <!-- Alert -->

                <!-- Services Columns -->
                <div class="services-columns white-bg">
                    <div id="services-slider" class="services-slider">

                        <!-- Service Figure -->
                        <div class="item">
                            <figure class="services-figure bg-1">
                                <figcaption>
                                    <h2>Basic Education</h2>
                                    <a class="tc-btn light shadow-0 sm" href="{{ route('basic.page') }}">Learn More</a>
                                </figcaption>
                                <img class="service-img" src="{{ asset('static/images/services/img-01.png') }}" alt="">
                            </figure>
                        </div>
                        <!-- Service Figure -->

                        <!-- Service Figure -->
                        <div class="item">
                            <figure class="services-figure bg-2">
                                <figcaption>
                                    <h2>Nursery</h2>
                                    <a class="tc-btn light shadow-0 sm" href="{{ route('nursery.page') }}">Learn
                                        More</a>
                                </figcaption>
                                <img class="service-img" src="{{ asset('static/images/services/img-02.png') }}" alt="">
                            </figure>
                        </div>
                        <!-- Service Figure -->

                        <!-- Service Figure -->
                        <div class="item">
                            <figure class="services-figure bg-3">
                                <figcaption>
                                    <h2>Creche</h2>
                                    <a class="tc-btn light shadow-0 sm" href="{{ route('creche.page') }}">Learn More</a>
                                </figcaption>
                                <img class="service-img" src="{{ asset('static/images/services/img-03.png') }}" alt="">
                            </figure>
                        </div>
                        <!-- Service Figure -->

                    </div>
                </div>
                <!-- Services Columns -->

            </div>
        </div>
    </section>
    <!-- Services -->



    <!-- Kids Img -->
    <div class="container">
        <div class="kids-img has-layout">

            <ul>
                <li class="animate swing" data-wow-delay="0.2s"><img
                        src="{{ asset('static/images/kids-imgs/img-01.png') }}" alt=""></li>
                <li class="animate lightSpeedIn" data-wow-delay="0.4s"><img
                        src="{{ asset('static/images/kids-imgs/img-02.png') }}" alt=""></li>
                <li class="animate swing" data-wow-delay="0.4s"><img
                        src="{{ asset('static/images/kids-imgs/img-03.png') }}" alt=""></li>
                <li class="animate lightSpeedIn" data-wow-delay="0.8s"><img
                        src="{{ asset('static/images/kids-imgs/img-04.png') }}" alt=""></li>
                <li class="animate fadeInRight" data-wow-delay="1s"><img
                        src="{{ asset('static/images/kids-imgs/img-05.png') }}" alt=""></li>
            </ul>
        </div>
    </div>
    <!-- Kids Img -->

    </section>
    <!-- School -->


    <!-- Team -->
    <section class="tc-padding-bottom">
        <img src="static/images/light-bg.png" alt="">
        <div class="container">

            <!-- Main Heading -->
            <div class="main-heading-holder">
                <div class="main-heading">
                    <h2>Our Expeienced Teachers</h2>
                </div>
            </div>
            <!-- Main Heading -->

            <!-- Team List -->
            <div id="team-slider" class="team-slider">

                <!-- Teachers Figure -->
                <div class="item">
                    <div class="team-figure">
                        <div class="aurthor-name">
                            <h3>Balogun Abiodun</h3>
                            <span>Head Mistress</span>
                        </div>
                        <figure>
                            <img src="{{ asset('static/images/team/img-01.jpg')}}" alt="Balogun Abiodun">
                        </figure>
                        <div class="on-hover">
                            <a href="mailto:" class="tc-btn"><i class="icon-envelope-o"></i> Email</a>
                        </div>
                    </div>
                </div>
                <!-- Teachers Figure -->

                <!-- Teachers Figure -->
                <div class="item">
                    <div class="team-figure">
                        <div class="aurthor-name">
                            <h3>Balogun Abiodun</h3>
                            <span>Head Mistress</span>
                        </div>
                        <figure>
                            <img src="{{ asset('static/images/team/img-01.jpg')}}" alt="Balogun Abiodun">
                        </figure>
                        <div class="on-hover">
                            <a href="mailto:" class="tc-btn"><i class="icon-envelope-o"></i> Email</a>
                        </div>
                    </div>
                </div>
                <!-- Teachers Figure -->

                <!-- Teachers Figure -->
                <div class="item">
                    <div class="team-figure">
                        <div class="aurthor-name">
                            <h3>Balogun Abiodun</h3>
                            <span>Head Mistress</span>
                        </div>
                        <figure>
                            <img src="{{ asset('static/images/team/img-01.jpg')}}" alt="Balogun Abiodun">
                        </figure>
                        <div class="on-hover">
                            <a href="mailto:" class="tc-btn"><i class="icon-envelope-o"></i> Email</a>
                        </div>
                    </div>
                </div>
                <!-- Teachers Figure -->


            </div>
            <!-- Team List -->

        </div>
    </section>
    <!-- Team -->

    <!-- Testimonial -->
    <section class="testimonial-holder"
        style="background: url(static/images/testimonial-bg.jpg) no-repeat center bottom;">
        <div class="container">
            <div class="row">
                <div class="col-lg-offset-1 col-lg-10">
                    <h3 class="curve-heading">Happy Parents</h3>
                    <span class="search-lable"><i class="icon-quotes-left"></i></span>
                    <ul id="testimonial-slider" class="testimonial-slider after-clear">

                        <!-- Teachers Figure -->
                        <li>
                            <div class="text">
                                <p>Lorem ipsum dolor sit amet, consectetur adcing elit Lorem ipsum dolor sit amet,
                                    consectetur adip iscing elit psum dolor sit amet. Aenean consectetur fringilla mi in
                                    mollis. Etiam eleifend sollicitudin dignissim.
                                    Lorem ipsum dolor sit amet, consectetur adcing elit Lorem ipsum dolor sit amet,
                                    consectetur adip iscing elit psum dolor sit amet. Aenean consectetur fringilla mi in
                                    mollis. Etiam eleifend sollicitudin dignissim.</p>

                                <div class="aurthor-name">
                                    <h3>Balogun Abiodun</h3>
                                </div>
                                <div class="img">
                                    <img src="static/images/kids-imgs/img-01.png" alt="Balogun Abiodun">
                                </div>
                            </div>
                        </li>
                        <!-- Teachers Figure -->

                        <!-- Teachers Figure -->
                        <li>
                            <div class="text">
                                <p>Lorem ipsum dolor sit amet, consectetur adcing elit Lorem ipsum dolor sit amet,
                                    consectetur adip iscing elit psum dolor sit amet. Aenean consectetur fringilla mi in
                                    mollis. Etiam eleifend sollicitudin dignissim.
                                    Lorem ipsum dolor sit amet, consectetur adcing elit Lorem ipsum dolor sit amet,
                                    consectetur adip iscing elit psum dolor sit amet. Aenean consectetur fringilla mi in
                                    mollis. Etiam eleifend sollicitudin dignissim.</p>

                                <div class="aurthor-name">
                                    <h3>Balogun Abiodun</h3>
                                </div>
                                <div class="img">
                                    <img src="static/images/kids-imgs/img-01.png" alt="Balogun Abiodun">
                                </div>
                            </div>
                        </li>
                        <!-- Teachers Figure -->



                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial -->


    <!-- Gallery -->
    <section class="tc-padding">
        <div class="container">

            <!-- Main Heading -->
            <div class="main-heading-holder">
                <div class="main-heading">
                    <h2>Our School Gallery</h2>
                    <p>Get to meet the kids up close and personal</p>
                </div>
            </div>
            <!-- Main Heading -->

            <!-- Gallery -->
            <div class="row">


                <!-- gallery Figure -->
                <div class="col-sm-3 col-sm-4 col-xs-6 r-full-width wow fadeInUp">
                    <figure class="gallery-figure rotate-1">
                        <img src="{{ asset('static/images/3.jpg') }}" alt="">
                        <figcaption class="overlay">
                            <h4 class="position-center-center"><a href="">Christmas Term 2017</a></h4>
                        </figcaption>
                    </figure>
                </div>
                <!-- gallery Figure -->

                <!-- gallery Figure -->
                <div class="col-sm-3 col-sm-4 col-xs-6 r-full-width wow fadeInUp">
                    <figure class="gallery-figure rotate-2">
                        <img src="{{ asset('static/images/2.jpg') }}" alt="">
                        <figcaption class="overlay">
                            <h4 class="position-center-center"><a href="">Summer Term 2017</a></h4>
                        </figcaption>
                    </figure>
                </div>
                <!-- gallery Figure -->


                <!-- gallery Figure -->
                <div class="col-sm-3 col-sm-4 col-xs-6 r-full-width wow fadeInUp">
                    <figure class="gallery-figure rotate-1">
                        <img src="{{ asset('static/images/3.jpg') }}" alt="">
                        <figcaption class="overlay">
                            <h4 class="position-center-center"><a href="">Halloween biscuits!</a></h4>
                        </figcaption>
                    </figure>
                </div>
                <!-- gallery Figure -->

                <!-- gallery Figure -->
                <div class="col-sm-3 col-sm-4 col-xs-6 r-full-width wow fadeInUp">
                    <figure class="gallery-figure rotate-2">
                        <img src="{{ asset('static/images/3.jpg') }}" alt="">
                        <figcaption class="overlay">
                            <h4 class="position-center-center"><a href="">Quiz Night Art 2016</a></h4>
                        </figcaption>
                    </figure>
                </div>
                <!-- gallery Figure -->

                <!-- gallery Figure -->
                <div class="col-sm-3 col-sm-4 col-xs-6 r-full-width wow fadeInUp">
                    <figure class="gallery-figure rotate-1">
                        <img src="{{ asset('static/images/3.jpg') }}" alt="">
                        <figcaption class="overlay">
                            <h4 class="position-center-center"><a href="">Christmas Term 2015</a></h4>
                        </figcaption>
                    </figure>
                </div>
                <!-- gallery Figure -->


                <!-- gallery Figure -->
                <div class="col-sm-3 col-sm-4 col-xs-6 r-full-width wow fadeInUp">
                    <figure class="gallery-figure rotate-2">
                        <img src="{{ asset('static/images/3.jpg') }}" alt="">
                        <figcaption class="overlay">
                            <h4 class="position-center-center"><a href="">Summer Term 2015</a></h4>
                        </figcaption>
                    </figure>
                </div>
                <!-- gallery Figure -->





            </div>
            <!-- Gallery -->

            <!-- Separater -->
            <span class="seprate-petrn mt-80"></span>
            <!-- Separater -->

        </div>
    </section>
    <!-- Gallery -->

    <!-- Latest Blogs -->
    <section class="tc-padding-bottom">
        <div class="container">

            <!-- Main Heading -->
            <div class="main-heading-holder">
                <div class="main-heading">
                    <h2>Latest News</h2>
                    <p>Keep up to date with all the latest news happening in school at the moment.</p>
                </div>
            </div>
            <!-- Main Heading -->

            <!-- Blogs -->
            <div id="blog-grid-slider" class="blog-grid-slider">


                <!-- Blog Column -->
                <div class="item">
                    <div class="blog-grid color-3">
                        <figure>
                            <img src="{{ asset('static/images/blogs-grid/img-02.jpg') }}" alt="blog Post">
                        </figure>
                        <div class="blog-detail">
                            <div class="meta-post">
                                <ul>
                                    <li><i class="icon-calendar"></i>March 1, 2019</li>
                                    {{-- <li><i class="icon-user"></i>Webmaster Nigeria</li> --}}
                                </ul>
                            </div>
                            <h4><a href="#">What To Do on the Second Day of Kindergarten</a></h4>
                            <span class="seprate-petrn white mb-10"></span>
                            <p>At vero eos et accusamu voluptatum similique sunt in culpa qui officia deserunt mollitia
                                animi, id est laborum et dolorum fuga...</p>
                        </div>
                    </div>
                </div>
                <!-- Blog Column -->



                <!-- Blog Column -->
                <div class="item">
                    <div class="blog-grid color-2">
                        <figure>
                            <img src="{{ asset('static/images/blogs-grid/img-01.jpg') }}" alt="blog Post">
                        </figure>
                        <div class="blog-detail">
                            <div class="meta-post">
                                <ul>
                                    <li><i class="icon-calendar"></i>March 1, 2019</li>
                                    {{-- <li><i class="icon-user"></i>Webmaster Nigeria</li> --}}
                                </ul>
                            </div>
                            <h4><a href="#">What To Do on the Second Day of Kindergarten</a></h4>
                            <span class="seprate-petrn white mb-10"></span>
                            <p>At vero eos et accusamu voluptatum similique sunt in culpa qui officia deserunt mollitia
                                animi, id est laborum et dolorum fuga...</p>
                        </div>
                    </div>
                </div>
                <!-- Blog Column -->

                <!-- Blog Column -->
                <div class="item">
                    <div class="blog-grid color-4">
                        <figure>
                            <img src="{{ asset('static/images/blogs-grid/img-03.jpg') }}" alt="blog Post">
                        </figure>
                        <div class="blog-detail">
                            <div class="meta-post">
                                <ul>
                                    <li><i class="icon-calendar"></i>March 1, 2019</li>
                                    {{-- <li><i class="icon-user"></i>Webmaster Nigeria</li> --}}
                                </ul>
                            </div>
                            <h4><a href="#">What To Do on the Second Day of Kindergarten</a></h4>
                            <span class="seprate-petrn white mb-10"></span>
                            <p>At vero eos et accusamu voluptatum similique sunt in culpa qui officia deserunt mollitia
                                animi, id est laborum et dolorum fuga...</p>
                        </div>
                    </div>
                </div>
                <!-- Blog Column -->

                <!-- Blog Column -->
                <div class="item">
                    <div class="blog-grid color-1">
                        <figure>
                            <img src="{{ asset('static/images/blogs-grid/img-03.jpg') }}" alt="blog Post">
                        </figure>
                        <div class="blog-detail">
                            <div class="meta-post">
                                <ul>
                                    <li><i class="icon-calendar"></i>March 1, 2019</li>
                                    {{-- <li><i class="icon-user"></i>Webmaster Nigeria</li> --}}
                                </ul>
                            </div>
                            <h4><a href="#">What To Do on the Second Day of Kindergarten</a></h4>
                            <span class="seprate-petrn white mb-10"></span>
                            <p>At vero eos et accusamu voluptatum similique sunt in culpa qui officia deserunt mollitia
                                animi, id est laborum et dolorum fuga...</p>
                        </div>
                    </div>
                </div>
                <!-- Blog Column -->



            </div>
            <!-- Blogs -->

        </div>
    </section>
    <!-- Latest Blogs -->

</main>
<!-- Main -->
@endsection



<!-- Services Columns -->
<div class="services-columns style-2 white-bg">
    <div id="services-slider" class="services-slider">

        <!-- Service Figure -->
        <div class="item">
            <figure class="services-figure bg-1">
                <figcaption>
                    <h2>Basic Education</h2>
                    <a class="tc-btn dark shadow-0 sm" href="{{ route('basic.page') }}">Learn More</a>

                </figcaption>
                <img class="service-img" src="{{ asset('static/images/services/img-01.png') }}" alt="">
            </figure>
        </div>
        <!-- Service Figure -->

        <!-- Service Figure -->
        <div class="item">
            <figure class="services-figure bg-2">
                <figcaption>
                    <h2>Nursery</h2>
                    <a class="tc-btn dark shadow-0 sm" href="{{ route('nursery.page') }}">Learn More</a>
                </figcaption>
                <img class="service-img" src="{{ asset('static/images/services/img-02.png') }}" alt="">
            </figure>
        </div>
        <!-- Service Figure -->

        <!-- Service Figure -->
        <div class="item">
            <figure class="services-figure bg-3">
                <figcaption>
                    <h2>Creche</h2>
                    <a class="tc-btn dark shadow-0 sm" href="{{ route('creche.page') }}">Learn More</a>
                </figcaption>
                <img class="service-img" src="{{ asset('static/images/services/img-03.png') }}" alt="">
            </figure>
        </div>
        <!-- Service Figure -->

    </div>
</div>
<!-- Services Columns -->