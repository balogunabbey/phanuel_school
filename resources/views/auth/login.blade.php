@extends('layouts.auth')
@section('title', 'User Authentication')
@section('content')
<style>
.reset {
    color: #9c0a2b;
}
.reset:hover {
    color: #771e32;
}
.error {
    color: #f00;
    font-size: 12px;
    /*margin-bottom:10px;*/
}
</style>

        <div class="wrapper">
            <div class="inner">
                <img src="{{ asset('static/images/auth/image-1.png')}}" alt="" class="image-1">
                <form action="{{ route('login') }}"  method="post">
                    @csrf
                <img class="login100-form-logo" src="{{ asset('static/images/ps_logo.jpg')}}" alt="" />
                    <h3>Welcome Back!</h3>

                    <div class="form-holder">
                        @error('email') 
                            <p class="error" role="alert">{{ $message }}</p>  
                        @enderror  
                        <span class="lnr lnr-user"></span>
                        <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Username" value="{{ old('email') }}">
                    </div>


                    <div class="form-holder">
                        @error('password') 
                        <p class="error" role="alert">{{ $message }}</p>  
                        @enderror
                        <span class="lnr lnr-lock"></span>
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                    </div>


                    <button>
                        <span>Login</span>
                    </button>
                </form>
                <img src="{{ asset('static/images/auth/image-2.png')}}" alt="" class="image-2">

            </div>
            
        </div>

<!-- content --> 
@endsection