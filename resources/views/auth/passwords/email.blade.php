@extends('layouts.auth')
@section('title', 'Forgot Password')
@section('content')
<form action="">
 {{ csrf_field() }}
<div class="content content-fixed content-auth-alt">
    <div class="container d-flex justify-content-center ht-100p">
        <div class="mx-wd-300 wd-sm-450 ht-100p d-flex flex-column align-items-center justify-content-center">
            {{-- <img style="width:200px; display: block; margin: 0 auto; width: 20%;" src="{{ asset('static/img/logo_full.png') }}"> --}}
        <div class="wd-80p wd-sm-300 mg-b-15 mt-4"><img src="{{ asset('static/img/app/forgot_password.png') }}" class="img-fluid" alt=""></div>
            <h4 class="tx-20 tx-sm-24">Forgot Password</h4>
            <p class="tx-color-03 mg-b-30 tx-center">Enter email address and we will send you a link to
                reset your password.</p>
                {{-- <form action="">
                   {{ csrf_field() }} --}}
            <div class="wd-100p d-flex flex-column flex-sm-row mg-b-40">
                <input type="text" class="form-control wd-sm-250 flex-fill" placeholder="Enter email address">
                <button class="btn btn-brand-02 mg-sm-l-10 mg-t-10 mg-sm-t-0">Reset Password</button>
            </div>

        </div>
    </div><!-- container -->
</div><!-- content -->
</form>
@endsection
