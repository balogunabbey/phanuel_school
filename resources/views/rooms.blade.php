@extends('layouts.app')
@section('title', 'Sessions &amp; Classes')
@section('content')
<style>
section {
    float: left;
    width: 100%;
    position: relative;
    }
    
    .block.no-padding {
    padding: 0;
    }
    
    .session-rooms {
    float: left;
    width: 100%;
    }
    
    .room-info {
    float: left;
    width: 100%;
    padding: 70px 30px 70px 60px;
    }
    
    .room-info.style2 {
    padding: 70px 60px 70px 30px;
    }
    
    .room-info.style3 {
    padding: 70px 30px 70px 60px;
    }
    
    
    .title-hd::before {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 155px;
    height: 4px;
    background-color: #5fcde3;
    }
    
    .room-info.style2 .title-hd::before {
    background-color: #d65190;
    }
    
    .title-hd {
    float: left;
    width: 100%;
    color: #575656;
    font-size: 38px;
    font-weight: 700;
    text-transform: uppercase;
    position: relative;
    padding-bottom: 33px;
    margin-bottom: 30px;
    }
    
    
    .room-info .title-hd {
    /* font-size: 53px; */
    margin-bottom: 25px;
    color: #575656;
    }
    
    .room-info span {
    color: #f1c00a;
    font-size: 18px;
    font-weight: 700;
    float: left;
    margin-bottom: 35px;
    width: 100%;
    }
    
    .room-info h3 {
    color: #37b8d2;
    font-size: 21px;
    font-weight: 700;
    line-height: 29px;
    margin-bottom: 23px;
    }
    
    .room-info.style2 h3 {
    color: #d65090;
    }
    
    .room-info.style3 h3 {
    color: #6b448e;
    }
    
    .room-info.style3 .title-hd::before {
    background-color: #765295;
    }
    
    .room-info p {
    font-size: 17px;
    line-height: 26px;
    margin-bottom: 30px;
    }
    
    .room-info>a {
    float: left;
    color: #ffffff;
    font-size: 18px;
    font-weight: 700;
    padding: 15px 60px;
    background-color: #36b6d0;
    background-image: url("../static/images/classes/bg-vector2.png");
    background-size: cover;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    -o-border-radius: 3px;
    border-radius: 3px;
    border: 2px solid #1a9eb9;
    }
    
    .room-info.style2 a {
    background-color: #cd4c89;
    border-color: #b63371;
    }
    
    .room-info.style3 a {
    background-color: #734f92;
    border-color: #5c367c;
    }
    
    .room-img {
    float: left;
    width: 100%;
    }
    
    .room-img img {
    width: 100%;
    height: 716px;
    object-fit: cover;
    }
    
    .block2 {
    padding: 60px 0;
    }
    
    .fixed-bg {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-size: cover;
    z-index: -1;
    background-repeat: no-repeat;
    }
    
    .bg-1 {
    background: #33b3cd url('../static/images/classes/bg-vector-lg.png');
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    }
    
    .bg-2 {
    background: #d24c8c url("../static/images/classes/bg-vector-lg.png");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    }
    
    .bg-3 {
    background: #714b91 url("../static/images/classes/bg-vector-lg.png");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    }
    
    .descp-sec {
    float: left;
    width: 100%;
    text-align: center;
    }
    
    .descp-sec h3 {
    color: #ffffff;
    font-size: 34px;
    font-weight: 700;
    }
    
    .descp-sec span {
    color: #ffdd00;
    }
    
    @media (max-width:769px) {
    .room-img img {
    height: 300px;
    }
    }
</style>

<!-- Inner Banner -->
<div class="inner-banner team" data-enllax-ratio="-.3"
style="background: url({{ asset('static/images/inner-banners/img-05.jpg') }}) 50% 0% no-repeat fixed;">
<div class="container">
<div class="inner-heading">
<h2>Sessions &amp; Classes</h2>
</div>
</div>
</div>
<!-- Inner Banner -->


<!-- Main -->
<main id="main">

<section class="block no-padding">
<div class="session-rooms">
<div class="row">
<div class="col-lg-3">
<div class="room-info">
<h3 class="title-hd">Creche classes</h3>
<span>AGES 0 - 2 Years</span>
<h3> Designed and equipped to promote physical skills such as crawling and walking. A beautiful and
stimulating environment with plenty of sensory experiences.</h3>
<p>Elipsis magna a terminal nulla elementum morbi elite forte maecenas est magna vehicula est node
maecenas.</p>
<a href="{{ route('creche.page') }}" title="Creche Classes">More Info</a>
</div>
<!--room-info end-->
</div>
<div class="col-lg-3 col-md-4 col-sm-4 col-4">
<div class="room-img">
<img src="{{ asset('static/images/classes/room1.jpg') }}" alt="">
</div>
<!--room-img end-->
</div>
<div class="col-lg-3 col-md-4 col-sm-4 col-4">
<div class="room-img">
<img src="{{ asset('static/images/classes/room2.jpg') }}" alt="">
</div>
<!--room-img end-->
</div>
<div class="col-lg-3 col-md-4 col-sm-4 col-4">
<div class="room-img">
<img src="{{ asset('static/images/classes/room3.jpg') }}" alt="">
</div>
<!--room-img end-->
</div>
</div>
</div>
<!--session-rooms end-->
</section>

<section class="block2">
<div class="fixed-bg bg-1"></div>
<div class="container">
<div class="descp-sec">
<h3>Over <span>30 Years Experience</span> in the childcare and development sector</h3>
</div>
<!--descp-sec end-->
</div>
</section>

<section class="block no-padding">
<div class="session-rooms">
<div class="row">
<div class="col-lg-3 col-md-4 col-sm-4 col-4">
<div class="room-img">
<img src="{{ asset('static/images/classes/room3.jpg') }}" alt="">
</div>
<!--room-img end-->
</div>
<div class="col-lg-3 col-md-4 col-sm-4 col-4">
<div class="room-img">
<img src="{{ asset('static/images/classes/room1.jpg') }}" alt="">
</div>
<!--room-img end-->
</div>
<div class="col-lg-3 col-md-4 col-sm-4 col-4">
<div class="room-img">
<img src="{{ asset('static/images/classes/room2.jpg') }}" alt="">
</div>
<!--room-img end-->
</div>
<div class="col-lg-3">
<div class="room-info style2">
<h3 class="title-hd">Nursery Classes</h3>
<span>AGES 0 - 2 Years</span>
<h3> Designed and equipped to promote physical skills such as crawling and walking. A beautiful and
stimulating environment with plenty of sensory experiences.</h3>
<p>Elipsis magna a terminal nulla elementum morbi elite forte maecenas est magna vehicula est node
maecenas.</p>
<a href="{{ route('nursery.page') }}" title="Nursery">More Info</a>
</div>
<!--room-info end-->
</div>
</div>
</div>
<!--session-rooms end-->
</section>

<section class="block2">
<div class="fixed-bg bg-2"></div>
<div class="container">
<div class="descp-sec">
<h3><span>Purpose built</span> play areas for your childs growth and education</h3>
</div>
<!--descp-sec end-->
</div>
</section>


<section class="block no-padding">
<div class="session-rooms">
<div class="row">
<div class="col-lg-3">
<div class="room-info style3">
<h3 class="title-hd">Basic Education Classes</h3>
<span>AGES 0 - 2 Years</span>
<h3> Designed and equipped to promote physical skills such as crawling and walking. A beautiful and
stimulating environment with plenty of sensory experiences.</h3>
<p>Elipsis magna a terminal nulla elementum morbi elite forte maecenas est magna vehicula est node
maecenas.</p>
<a href="{{ route('basic.page') }}" title="">More Info</a>
</div>
<!--room-info end-->
</div>
<div class="col-lg-3 col-md-4 col-sm-4 col-4">
<div class="room-img">
<img src="{{ asset('static/images/classes/room1.jpg') }}" alt="">
</div>
<!--room-img end-->
</div>
<div class="col-lg-3 col-md-4 col-sm-4 col-4">
<div class="room-img">
<img src="{{ asset('static/images/classes/room3.jpg') }}" alt="">
</div>
<!--room-img end-->
</div>
<div class="col-lg-3 col-md-4 col-sm-4 col-4">
<div class="room-img">
<img src="{{ asset('static/images/classes/room2.jpg') }}" alt="">
</div>
<!--room-img end-->
</div>
</div>
</div>
<!--session-rooms end-->
</section>

<section class="block2">
<div class="fixed-bg bg-3"></div>
<div class="container">
<div class="descp-sec">
<h3><span>Fully skilled</span> staff and teachers to get your child ready for primary school</h3>
</div>
<!--descp-sec end-->
</div>
</section>






</main>
<!-- Main -->
@endsection