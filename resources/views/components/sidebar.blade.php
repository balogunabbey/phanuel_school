<!-- Sidebar Area Start Here -->
<div class="sidebar-main sidebar-menu-one sidebar-expand-md sidebar-color">
    <div class="mobile-sidebar-header d-md-none">
        <div class="header-logo">
            <a href="{{ route('dashboard.index')}}"><img src="{{ asset('static/auth/img/logo1.png')}}" alt="logo"></a>
        </div>
    </div>
    <div class="sidebar-menu-content">
        <ul class="nav nav-sidebar-menu sidebar-toggle-view">


            <li class="nav-item">
                <a href="{{ route('dashboard.index')}}" class="nav-link"><i
                        class="flaticon-dashboard"></i><span>Dashboard</span></a>
            </li>


            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="flaticon-classmates"></i><span>My Kids</span></a>
                <ul class="nav sub-group-menu">
                    <li class="nav-item">
                        <a href="{{ route('kids.index')}}" class="nav-link"><i class="fas fa-angle-right"></i>Registered
                            Kids</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('enrol.new')}}" class="nav-link"><i class="fas fa-angle-right"></i>Enrol Your
                            Child</a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route('tuition.index')}}" class="nav-link"><i class="fas fa-angle-right"></i>School
                            Fees / Tuition</a>
                    </li>

                </ul>
            </li>


            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="flaticon-user"></i><span>Pickup Assistant</span></a>
                <ul class="nav sub-group-menu">
                    <li class="nav-item">
                        <a href="{{ route('pickup.index')}}" class="nav-link"><i class="fas fa-angle-right"></i>All
                            Pickup Assistant(s)</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('pickup.new')}}" class="nav-link"><i class="fas fa-angle-right"></i>Add Pickup
                            Assistant</a>
                    </li>

                </ul>
            </li>


            <li class="nav-item ">
                <a href="{{ route('grade.index')}}" class="nav-link"><i class="flaticon-mortarboard"></i><span>Result
                        Checker</span></a>
            </li>




            <li class="nav-item ">
                <a href="{{ route('absence.index')}}" class="nav-link"><i class="flaticon-checklist"></i><span>Absence
                        Requests</span></a>
            </li>



            <li class="nav-item ">
                <a href="" class="nav-link"><i class="flaticon-bus-side-view"></i><span>School Transport</span></a>
            </li>



            <li class="nav-item ">
                <a href="{{ route('notice.index')}}" class="nav-link"><i class="flaticon-ring"></i><span>Important
                        Notice</span></a>
            </li>


            <li class="nav-item ">
                <a href="#" class="nav-link"><i class="flaticon-script"></i><span>FAQs</span></a>
            </li>


            <li class="nav-item ">
                <a href="{{ route('open.ticket')}}" class="nav-link"><i class="flaticon-chat"></i><span>Reports /
                        Suggestions</span></a>
            </li>


        </ul>
    </div>
</div>
<!-- Sidebar Area End Here -->