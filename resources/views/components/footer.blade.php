<!-- jquery-->
<script src="{{ asset('static/auth/js/jquery-3.3.1.min.js')}}"></script>
<!-- Plugins js -->
<script src="{{ asset('static/auth/js/plugins.js')}}"></script>
<!-- Popper js -->
<script src="{{ asset('static/auth/js/popper.min.js')}}"></script>
<!-- Bootstrap js -->
<script src="{{ asset('static/auth/js/bootstrap.min.js')}}"></script>
<!-- Select 2 Js -->
<script src="{{ asset('static/auth/js/select2.min.js')}}"></script>
<!-- Date Picker Js -->
<script src="{{ asset('static/auth/js/datepicker.min.js')}}"></script>
<!-- Counterup Js -->
<script src="{{ asset('static/auth/js/jquery.counterup.min.js')}}"></script>
<!-- Waypoints Js -->
<script src="{{ asset('static/auth/js/jquery.waypoints.min.js')}}"></script>
<!-- Scroll Up Js -->
<script src="{{ asset('static/auth/js/jquery.scrollUp.min.js')}}"></script>
<!-- Data Table Js -->
<script src="{{ asset('static/auth/js/jquery.dataTables.min.js')}}"></script>
<!-- Custom Js -->
<script src="{{ asset('static/auth/js/main.js')}}"></script>
<!-- Full Calender Js -->
<script src="{{ asset('static/auth/js/fullcalendar.min.js')}}"></script>
<!-- Chart Js -->
<script src="{{ asset('static/auth/js/Chart.min.js')}}"></script>
@include('alert.alert')
</body>

</html>