@extends('layouts.app')
@section('title', 'Welcome')
@section('content')
<!-- Banner Slider -->
<div id="banner-slider" class="banner-slider">
    <img class="sp-image" src="{{asset('static/images/slider/bg.jpg')}}" alt="">

    <!-- Slides -->
    <div class="sp-slides">

        <!-- Slide 1 -->
        <div class="sp-slide">
            <!-- Caption -->
            <div class="sp-layer sp-padding caption-text" data-horizontal="271" data-vertical="40"
                data-show-transition="left" data-hide-transition="left" data-show-delay="600" data-hide-delay="700">
                <div class="banner-caption">
                    <span>Welcome to</span>
                    <h1>PHANUEL SCHOOLS</h1>
                    <p>Here is an exciting, motivating and inclusive school<br> where every individual, old and young,
                        is valued.<br> We believe that education is a partnership between home and<br> school and we try
                        to involve parents in all aspects of school life.</p>
                    <a class="tc-btn" href="">Discover More</a>
                </div>
            </div>
            <!-- Caption -->
            <!-- Banner Layers -->
            {{-- <img class="sp-layer layer-img-1" data-horizontal="919" data-vertical="-5" data-show-transition="right" data-hide-transition="up" data-show-delay="400" data-hide-delay="200" src="{{ asset('static/images/slider/.png') }}"
            alt=""> --}}
            <img class="sp-layer layer-img-2" data-horizontal="750" data-vertical="-5" data-show-transition="up"
                data-hide-transition="up" data-show-delay="800" data-hide-delay="500"
                src="{{ asset('static/images/slider/slider-layer-2.png') }}" alt="">
            <!-- Banner Layers -->
        </div>
        <!-- Slide 1 -->

        <!-- Slide 1 -->
        <div class="sp-slide">
            <!-- Caption -->
            <div class="sp-layer sp-padding caption-text" data-horizontal="271" data-vertical="40"
                data-show-transition="left" data-hide-transition="left" data-show-delay="600" data-hide-delay="700">
                <div class="banner-caption">
                    <span>At</span>
                    <h1>PHANUEL SCHOOLS</h1>
                    <p>We aim to create an environment where each child is made to <br><span>feel secure, wanted and
                            valued. we make every<br> effort to produce a happy atmosphere with mutual respect<br> shown
                            by everyone in our school family.</span></p>
                    <a class="tc-btn" href="../admissions">Get Enroll Now</a>
                </div>
            </div>
            <!-- Caption -->
            <!-- Banner Layers -->
            {{-- <img class="sp-layer layer-img-1" data-horizontal="919" data-vertical="-5" data-show-transition="right" data-hide-transition="up" data-show-delay="400" data-hide-delay="200" src="{{ asset('static/images/slider/img-01.png') }}"
            alt=""> --}}
            <img class="sp-layer layer-img-2" data-horizontal="750" data-vertical="-5" data-show-transition="up"
                data-hide-transition="up" data-show-delay="800" data-hide-delay="500"
                src="{{ asset('static/images/slider/slider-layer-1.png') }}" alt="">
            <!-- Banner Layers -->
        </div>
        <!-- Slide 1 -->



    </div>
    <!-- Slides -->

    <!-- Paginaition dots -->
    <ul class="sp-thumbnails">
        <li class="sp-thumbnail"></li>
        <li class="sp-thumbnail"></li>
    </ul>
    <!-- Paginaition dots -->

</div>
<!-- Banner Slider -->

<!-- Main -->
<main id="main">

    <style>
  
        </style>

    <section class="call-to-action" style="background-image: url({{ asset('static/img/about_banner.jpg') }});">
        <div class="auto-container">
            <div class="inner-container">
                <h2>We give our children a learning environment & contribute to their success!</h2>
                <div class="link-box clearfix">
                <a href="{{ route('about.page') }}" class="pull-left">Learn About Our School</a>
                   <span class="icon icon-starz wow zoomInStable"></span>
                    <a href="{{ route('classes.page') }}" class="pull-right">Our Classes</a>
                </div>
            </div>
        </div>
    </section>


    
<section class="facilites-section">
    <div class="container">
        <div class="row clearfix">

            <!-- Info Column -->
            <div class="info-column col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <span class="icon doll-3"></span>
                    <div class="sec-title">
                        <h2><span class="text-parrot">School</span> Facilities</h2>
                    </div>
                    <h3>EveryDay Care for your Children ...</h3>
                    <p>Dolor sit amet consectetur elit eiusmod tempor dunt aliqua utas enim veniam tempore quis sed
                        ipsum nostrud ipsume amet consectetur adipisicing elit sedo eiusmod tempor incididunt ut labore
                        et dolore magna aliquat enim.</p>
                    <div class="facilities">
                        <div class="row clearfix">
                            <div class="facility-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                <div class="image-box"><img src="{{ asset('static/img/facility-1.png')}}" alt=""></div>
                                    <h4>Clean Playgrounds</h4>
                                </div>
                            </div>

                            <div class="facility-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image-box"><img src="{{ asset('static/img/facility-2.png')}}" alt=""></div>
                                    <h4>Private School Bus</h4>
                                </div>
                            </div>

                            <div class="facility-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image-box"><img src="{{ asset('static/img/facility-3.png')}}" alt=""></div>
                                    <h4>Modern Canteen</h4>
                                </div>
                            </div>

                            <div class="facility-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image-box"><img src="{{ asset('static/img/facility-4.png')}}" alt=""></div>
                                    <h4>Colorful Classes</h4>
                                </div>
                            </div>

                            <div class="facility-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image-box"><img src="{{ asset('static/img/facility-5.png')}}" alt=""></div>
                                    <h4>Positive Learning</h4>
                                </div>
                            </div>

                            <div class="facility-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image-box"><img src="{{ asset('static/img/facility-6.png')}}" alt=""></div>
                                    <h4>Fun With Games</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Image Column -->
    <div class="image-column">
        <div class="inner-column">
            <div class="layer-image" style="background-image: url({{ asset('static/img/image-1.jpg') }}"></div>
            <div class="image-box">
            <img src="{{ asset('static/img/image-1.jpg') }}" alt="">
            </div>
        </div>
    </div>
</section>


  

   <!-- Team List -->
    <section class="tc-padding-bottom">
        <div class="staff-section container">
            <div class="sec-title text-center">
                <span class="icon-1 doll-5"></span>
                <span class="icon-2 icon-pencil-2"></span>
                <h2><span class="text-sky">Amazing</span> Teachers</h2>
            </div>

            <!-- Main Heading -->

            <div class="row clearfix">
                <!-- Staff Block -->
                <div class="staff-block col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box wow fadeInDown" data-wow-duration="1500ms" data-wow-delay="0ms"><a href="#"><img
                                    src="{{ asset('static/images/team/img-01.jpg')}}" alt=""></a></div>
                        <div class="info-box">
                            <h3><a href="{{ route('staffs.page') }}">Balogun Abiodun</a></h3>
                            <p>Founder &amp; Head</p>
                        </div>
                    </div>
                </div>
            
                <!-- Staff Block -->
                <div class="staff-block col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box wow fadeInDown" data-wow-duration="1500ms" data-wow-delay="300ms"><a href="#"><img
                                    src="{{ asset('static/images/team/img-01.jpg')}}" alt=""></a></div>
                        <div class="info-box">
                            <h3><a href="{{ route('staffs.page') }}">Sodiq Obalowo</a></h3>
                            <p>Sports Instructor</p>
                        </div>
                    </div>
                </div>
            
                <!-- Staff Block -->
                <div class="staff-block col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box wow fadeInDown" data-wow-duration="1500ms" data-wow-delay="600ms"><a href="#"><img
                                    src="{{ asset('static/images/team/img-01.jpg')}}" alt=""></a></div>
                        <div class="info-box">
                        <h3><a href="{{ route('staffs.page') }}">Korede Afolayan</a></h3>
                            <p>Primary Teacher</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Team List -->

        </div>
    </section>
    <!-- Team -->

<!-- Video Section -->
    <section class="video-section" style="background-image: url(static/img/video_banner.jpg);">
        <div class="container">
            <div class="content-box clearfix">
                <div class="title-box">
                    <h2>Watch The Campus Study Video Tour</h2>
                    <p>Labore et dolore magna aliqua ut enim adus minim veniam quis nostrud exercitation ullamco</p>
                </div>
                <div class="btn-box">
                    <a target="_blank" href="https://www.youtube.com/watch?v=Fvae8nxzVz4" class="play-btn" data-fancybox="gallery"
                        data-caption=""><span class="play-icons"></span>Play Video </a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Video Section -->



<!-- Latest Blogs -->    
<section class="news-section">
        <div class="container">
            <div class="sec-title text-center">
                <span class="icon-1 icon-star-2"></span>
                <span class="icon-2 doll-3"></span>
                <h2><span class="text-parrot">News &amp;</span> Events</h2>
            </div>
    
            <!-- News Block -->
            <div class="news-block">
                <div class="row clearfix">
                    <div class="content-column col-md-7 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            <div class="image-column col-md-7 col-sm-7 col-xs-12">
                                <div class="image">
                                <a href="{{ route('newspreview.page') }}"><img src="{{ asset('static/img/news-1.jpg') }}" alt=""></a>
                                </div>
                            </div>
    
                            <div class="info-column col-md-5 col-sm-5 col-xs-12">
                                <div class="inner-column">
                                    <h3 class="date">15 Nov</h3>
                        <h4><a href="{{ route('newspreview.page') }}">Celebrating Democracy Day Tomorrow</a></h4>                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="detail-column col-md-5 col-sm-12 col-xs-12">
                        <div class="inner-column">
                            <p>Dolor sit amet consectetur elit eiusmod tempor dunt aliqua enim veniam tempore quis sed ipsum
                                nostrud ipsume amet onsectetur adipisicing elit sedo eiusmod.</p>
                            <div class="btn-box">
                                <a href="{{ route('newspreview.page') }}" class="theme-btn btn-style-twp read-more">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                        <!-- News Block -->
            <div class="news-block">
                <div class="row clearfix">
                    <div class="content-column col-md-7 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            <div class="image-column col-md-7 col-sm-7 col-xs-12">
                                <div class="image">
                                <a href="{{ route('newspreview.page') }}"><img src="{{ asset('static/img/news-1.jpg') }}" alt=""></a>
                                </div>
                            </div>
    
                            <div class="info-column col-md-5 col-sm-5 col-xs-12">
                                <div class="inner-column">
                                    <h3 class="date">15 Nov</h3>
                        <h4><a href="{{ route('newspreview.page') }}">Celebrating Democracy Day Tomorrow</a></h4>                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="detail-column col-md-5 col-sm-12 col-xs-12">
                        <div class="inner-column">
                            <p>Dolor sit amet consectetur elit eiusmod tempor dunt aliqua enim veniam tempore quis sed ipsum
                                nostrud ipsume amet onsectetur adipisicing elit sedo eiusmod.</p>
                            <div class="btn-box">
                                <a href="{{ route('newspreview.page') }}" class="theme-btn btn-style-twp read-more">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
    </section>
    <!-- Latest Blogs -->

<!-- Enrol Now -->
{{-- <style>
.call-out.type-2 {
padding: 30px 0;
/* background: #714b91; */
background: #714b91 url('http://127.0.0.1:8001/static/images/classes/bg-vector-lg.png');
}
.table-row {
margin-left: 0px;
margin-right: 0px;
display: table;
width: 100%;
table-layout: fixed;
}

.call-out h2:not(:last-child) {
margin-bottom: 7px;
}
.call-out h2 {
padding-top: 10px;
font-size: 40px;
line-height: 42px;
color: #fff;
}

.call-out p {
color: #858585;
}

.align-right {
text-align: right;
}

.call-out .button-holder {
display: inline-block;
text-align: center;
}

.call-out .button-holder > .call-btn:not(:last-child) {
margin-bottom: 10px;
}
.btn-big {
padding: 14px 30px;
font-size: 18px;
}

.call-btn {
padding: 10px 20px 8px;
display: inline-block;
font-size: 14px;
border-radius: 30px;
background: #f05a21;
color: #fff;
text-align: center;
line-height: 24px;
letter-spacing: 0.75px;
-webkit-box-shadow: 0px 5px 25px 0px rgba(240, 90, 33, 0.35);
-moz-box-shadow: 0px 5px 25px 0px rgba(240, 90, 33, 0.35);
box-shadow: 0px 5px 25px 0px rgba(240, 90, 33, 0.35);
}

.call-out .button-holder p {
color: #b9b9b9;
}

.call-out p > span {
color: #fff;
text-transform: uppercase;
font-weight: 600;
}

@media (max-width:769px){
.call-us > *:not(:last-child){
margin-right: 20px;
}

.call-out,
.call-out .align-right{
text-align: center;
}

.call-out h2{
font-size: 32px;
}

.call-out [class*="col-sm-"]:not(:last-child){
margin-bottom: 20px;
}
}
</style>
<div class="call-out type-2">
        
            <div class="container">
        
                <div class="row table-row">
                    <div class="col-sm-8">
        
                        <h2>Enrol Your Child For 2018-2019 Academic Session</h2>
        
                    </div>
                    <div class="col-sm-4">
        
                        <div class="align-right">
                            <div class="button-holder">
                                <a href="#" class="call-btn btn-big">Enrol Now!</a>
                                <p>Or Call <span>0802 849 5250</span></p>
                            </div>
                        </div>
        
                    </div>
                </div>
        
            </div>
        
        </div> --}}
<!-- Enrol Now -->

</main>
<!-- Main -->
@endsection