@extends('layouts.app')
@section('title', 'Privacy Policy') 
@section('content')

<!-- Inner Banner -->
<div class="inner-banner gallery text-center" data-enllax-ratio="-.3" style="background: url({{ asset('static/images/inner-banners/img-04.jpg')}}) 50% 0% no-repeat fixed;">
<div class="container">
<div class="inner-heading">
<h2>Privacy Policy</h2>
</div>
</div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

<!-- Gallery Holder -->
<section class="gallery-holder tc-padding-bottom gray-bg">
<div class="container">
<div class="content has-layout">

<!-- Breadcrumbs -->
<div class="breadcrumbs">
<ul>
<li><i class="icon-folder"></i> Privacy Policy</li>
<li><a href="{{ route('home.page') }}"><i class="icon-home22"></i> Back to Home</a></li>
</ul>
</div>
<!-- Breadcrumbs -->

<!-- Gallery -->
<div class="gallery">

<!-- Gallery Tabs Panels -->
<div class="gallery-panel has-layout">
<div class="tab-pane active" id="tab1">

<!-- Main Heading -->
<div class="main-heading-holder">
<div class="main-heading">
<p style="text-align:justify;">Your privacy is important to us. To better protect your privacy we provide this notice explaining our information practices and the choices you can 
make about the way your information is collected and used. To make this notice easy to find, we make it available on every pages of our website.</p>
<div class="flat-divider d20px"></div>

<div class="sec-title">
<h3>The information we collect</h3>
</div>

<p style="text-align:justify;">This notice applies to all information collected or submitted on the  Phanuel School ’s website or via phone with a  Phanuel School's  representative. The types of information collected 
are personal, billing and project related. We might also collect information from you regarding third parties in relation to you (i.e. hosting company etc.)</p>
<div class="flat-divider d20px"></div>


<div class="sec-title">
<h3>The way we use information</h3>
</div>

<p style="text-align:justify;">We use the information you provide about yourself to provide services. We do not share this information with outside parties except to the extent necessary to complete your 
request for our services.  Phanuel School  reserves the right to reveal this information pursuant to a valid subpoena or court order.We use return email addresses to answer the
email we receive for tech support, customer service, tell-a-friend, email updates. Such addresses are not used for any other purpose and are not shared with outside parties. Finally, we never use or share the personally identifiable information provided to us online, via phone, or in person, in ways unrelated to the ones described
above without also providing you an opportunity to opt-out or otherwise prohibit such unrelated uses.</p>
<div class="flat-divider d20px"></div>

<div class="sec-title">
<h3>Our commitment to children’s privacy</h3>
</div>
<div class="flat-divider d20px"></div>

<p style="text-align:justify;">This website may provide links to third-party websites. Any such links are provided solely as a convenience to you.  Phanuel School  has no control over these websites or their content 
and does not assume any responsibility or liability for these websites.  Phanuel School  does not endorse or make any representations about these websites, or any information, materials,
or products found thereon. If you access any of the third-party websites linked on this Web site, you do so at your own risk.</p>
<div class="flat-divider d20px"></div>

<div class="sec-title">
<h3>How you can access or correct your information</h3>
</div>
<div class="flat-divider d20px"></div>

<p style="text-align:justify;">At any time, you may later review or update the personal information we have collected online from you by contacting us at <a style="color:#56d92d;" href="mailto:hello@phanuelschools.sch.ng">hello@phanuelschools.sch.ng</a>. To protect your privacy and security, we will also take reasonable steps to verify your identity before granting access or making corrections.</p>
<div class="flat-divider d20px"></div>

<div class="sec-title">
<h3>How you can access or correct your information</h3>
</div>
<div class="flat-divider d20px"></div>

<p style="text-align:justify;">Should you have other questions or concerns about these privacy policies, please contact us via email at <a style="color:#56d92d;" href="mailto:hello@phanuelschools.sch.ng">hello@phanuelschools.sch.ng</a></p>   </div>
</div>
<!-- Main Heading -->

<!-- Thumbnail -->

<!-- Thumbnail -->

</div>
</div>	
<!-- Gallery Tabs Panels -->

</div>
<!-- Gallery -->

</div>
</div>
</section>
<!-- Gallery Holder -->

</main>
<!-- Main -->    
@endsection