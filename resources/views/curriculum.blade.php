@extends('layouts.app')
@section('title', 'Curriculum')
@section('content')

<!-- Inner Banner -->
<div class="inner-banner team" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-05.jpg') }}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>Curriculum</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

    <!-- Blog Detail -->
    <section class="blogs-holder style-2 tc-padding-bottom">
        <div class="container">

            <!-- Content -->
            <div class="content has-layout">

                <!-- Breadcrumbs -->
                <div class="breadcrumbs">
                    <ul>
                        <li><i class="icon-folder"></i> Pupils</li>
                        <li>Curriculum</li>
                        <li><a href="{{ route('home.page') }}"><i class="icon-home22"></i> Back to Home</a></li>
                    </ul>
                </div>
                <!-- Breadcrumbs -->

                <div class="single-blog-detail">
                    <div class="single-blog-title">
                        <h2>Curriculum</h2>
                    </div>

                    <div class="blog-article">
                        <p>The aim of the school is to provide a broad and balanced curriculum which will enable
                            children to develop intellectually, aesthetically, physically, socially, morally and
                            spiritually.</p>

                        <p>The school aims to provide first-hand learning experiences in which children are
                            self-motivated and interested, through which they will acquire basic skills and each develop
                            to their full potential. Particular emphasis is placed on children acquiring literacy and
                            numeracy. A daily literacy and numeracy session takes place in each class. The children's
                            work is planned so that they may proceed at their own rate within their level of ability. On
                            some occasions children gather as a class, or in groups, and on other occasions an
                            individual approach will be taken.</p>
                    </div>

                    <blockquote>
                        <p>The core subjects of the National Curriculum are English, Maths and Science.</p>
                        <span class="search-lable"><i class="icon-star-full"></i></span>
                    </blockquote>

                    <div class="blog-article style-2">
                        <p>The school gives regular homework to all Infant children, comprising daily reading and weekly
                            Maths and English work. This is often of an oral or practical, rather than written nature.
                        </p>
                    </div>

                    <div class="school-fecilities dot-heading mb-20 has-layout">
                        <div class="see-also dot-heading mt-20 has-layout">
                            <h3>The areas of study in the Foundation Stage, which lead to the achievement of the Early
                                Learning Goals are:</h3>
                            <ul class="see-also-list">
                                <li>Personal, Social and Emotional Development</li>
                                <li>Literacy</li>
                                <li>Maths</li>
                                <li>Understanding of the World</li>
                                <li>Physical Development</li>
                                <li>Expressive Arts and Design</li>
                            </ul>
                        </div>
                    </div>

                    <div class="teacher-quotes style-2 has-layout">
                        <div class="quotes">
                            <p>We have well-packaged extra-curricular activities to complement and enhance classroom
                                experiences and provide pupils/students with opportunities for personal growth and
                                enrichment. Phanuel Schools have what you are looking for.</p>
                            <ul class="share-btn btn-list">
                                <li><a class="tc-btn shadow-0 facebook" href="">Register Now</a>
                            </ul>
                        </div>
                        <img class="girl-layer" src="{{ asset('static/images/slant.png')}}" height="400" alt="">
                    </div>




                </div>
            </div>
            <!-- Content -->

        </div>
    </section>


</main>
<!-- Main -->
@endsection