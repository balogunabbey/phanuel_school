@extends('layouts.app')
@section('title', 'Share Your Story')
@section('content')


<!-- Inner Banner -->
<div class="inner-banner team text-center" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-07.jpg') }}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>Testimonials</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

    <!-- Blog Detail -->
    <section class="blogs-holder tc-padding-bottom gray-bg">
        <div class="container">
            <div class="row">

                <!-- Content -->
                <div class="col-lg-8 col-md-8 col-xs-12">


                    <div class="content has-layout">

                        <!-- Breadcrumbs -->
                        <div class="breadcrumbs">
                            <ul>
                                <li><i class="icon-folder"></i> Parents</li>
                                <li>Share Your Story</li>
                                <li><a href="{{ route('home.page') }}"><i class="icon-home22"></i> Back to Home</a></li>
                            </ul>

                        </div>
                        {{-- </div> --}}
                        <!-- Breadcrumbs -->


                        <div class="single-blog-detail">

                            <!-- Leave Comment -->
                            <div class="leave-comment has-layout">
                                <h3>We love to hear your experience with us</h3>
                                <br>

                                <form class="comment-form" action="{{ route('share.testimony') }}" method="post"
                                    data-parsley-validate enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label>Your Full Name<br>
                                            @if ($errors->has('full_name'))
                                            <span
                                                style="color:red; font-size:10px; text-transform: capitalize;">{{ $errors->first('full_name') }}</span>
                                            @endif
                                        </label>
                                        <input type="text" data-parsley-required class="form-control"
                                            placeholder="John Doe" name="full_name">
                                    </div>


                                    <div class="form-group">
                                        <label>Your Email <br>
                                            @if ($errors->has('your_email'))
                                            <span
                                                style="color:red; font-size:10px; text-transform: capitalize;">{{ $errors->first('your_email') }}</span>
                                            @endif
                                        </label>
                                        <input type="text" data-parsley-required class="form-control"
                                            placeholder="name@email.com" name="your_email">
                                    </div>


                                    <div class="form-group">
                                        <label>Phone Number <br>
                                            @if ($errors->has('your_phone'))
                                            <span
                                                style="color:red; font-size:10px; text-transform: capitalize;">{{ $errors->first('your_phone') }}</span>
                                            @endif
                                        </label>
                                        <input type="text" data-parsley-type="digits" class="form-control"
                                            placeholder="123 456 78910" name="your_phone">
                                    </div>


                                    <div class="form-group">
                                        <textarea class="form-control" rows="10" placeholder="Share Your Opinion"
                                            name="your_opinion"></textarea>
                                        @if ($errors->has('your_opinion'))
                                        <span class="m-2"
                                            style="color:red; font-size:10px; text-transform: capitalize; margin:5px !important;">{{ $errors->first('your_opinion') }}</span>
                                        @endif
                                    </div>


                                    <button class="tc-btn full-with shadow-0" name="tag2">Share Testimonial</button>
                                </form>
                            </div>
                            <!-- Leave Comment -->

                        </div>

                    </div>

                </div>
                <!-- Content -->

                <!-- Aside -->
                <aside class="col-lg-4 col-md-4 col-xs-12">
                    <div class="blog-aside row">

                        <!-- FaceBook -->
                        <div class="aside-widget">
                            <h3>Facebook</h3>
                            <iframe
                                src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fphoto.php%3Ffbid%3D104681879998821%26set%3Da.104681913332151.1073741827.100013710274610%26type%3D3%26theater&width=500&show_text=true&height=439&appId"
                                width="400" height="400" style="border:none;overflow:hidden" scrolling="no"
                                frameborder="0" allowTransparency="true"></iframe>
                        </div>
                        <!-- FaceBook -->

                    </div>
                </aside>
                <!-- Aside -->

            </div>
        </div>
    </section>


</main>
<!-- Main -->
@endsection