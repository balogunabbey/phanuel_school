@extends('layouts.app')
@section('title', 'News &amp; Events') 
@section('content')

<!-- Inner Banner -->
<div class="inner-banner blog text-center" data-enllax-ratio="-.3" style="background: url({{ asset('static/images/inner-banners/img-03.jpg')}}) 50% 0% no-repeat fixed;">
<div class="container">
<div class="inner-heading">
<h2>How to clean your teeth in the morning</h2>
</div>
</div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

<!-- Blog Detail -->
<section class="blogs-holder tc-padding-bottom gray-bg">
<div class="container">
<div class="row">

<!-- Content -->
<div class="col-lg-8 col-md-8 col-xs-12">

<!-- Blog Content -->
<div class="content has-layout">

<!-- Breadcrumbs -->
<div class="blog-detail-breadcrumbs">
<div class="blog-date">
<h5>12</h5>
<span>May<i>2019</i></span>
</div>
<div class="tag-nd-categories">
<ul>
<li><a href="{{ route('news.page') }}"><i class="icon-folder"></i>News and Events</a></li>
</ul>
<a class="back-home" href="{{ route('home.page') }}"><i class="icon-home"></i>Back to Home</a>
</div>
</div>
<!-- Breadcrumbs -->

<!-- Blogs Detail -->
<div class="single-blog-detail">

<!-- Single Blog Title -->
<div class="single-blog-title">
<h2>How to clean your teeth in the morning</h2>
</div>
<!-- Single Blog Title -->

<!-- Blog Article -->
<div class="blog-article">
<p>The school aims to provide first-hand learning experiences in which children are self-motivated and interested, through which they will acquire basic skills and each develop to their full potential. Particular emphasis is placed on children acquiring literacy and numeracy. A daily literacy and numeracy session takes place in each class. The children's work is planned so that they may proceed at their own rate within their level of ability. On some occasions children gather as a class, or in groups, and on other occasions an individual approach will be taken.</p>
<p>The school aims to provide first-hand learning experiences in which children are self-motivated and interested, through which they will acquire basic skills and each develop to their full potential. Particular emphasis is placed on children acquiring literacy and numeracy. A daily literacy and numeracy session takes place in each class. The children's work is planned so that they may proceed at their own rate within their level of ability. On some occasions children gather as a class, or in groups, and on other occasions an individual approach will be taken.</p>
<p>The school aims to provide first-hand learning experiences in which children are self-motivated and interested, through which they will acquire basic skills and each develop to their full potential. Particular emphasis is placed on children acquiring literacy and numeracy. A daily literacy and numeracy session takes place in each class. The children's work is planned so that they may proceed at their own rate within their level of ability. On some occasions children gather as a class, or in groups, and on other occasions an individual approach will be taken.</p>
</div>
<!-- Blog Article -->

</div>
<!-- Blogs Detail -->

</div>
<!-- Blog Content -->

</div>
<!-- Content -->

<!-- Aside -->
<aside class="col-lg-4 col-md-4 col-xs-12">
<div class="blog-aside">

<!-- Add -->
{{-- <div class="aside-widget add">
<a href="#"><img src="{{ asset('static/images/add-3.jpg') }}" alt=""></a>
</div> --}}
<!-- Add -->


<!-- Add -->
<div class="aside-widget">
<h3>Facebook</h3>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fphoto.php%3Ffbid%3D104681879998821%26set%3Da.104681913332151.1073741827.100013710274610%26type%3D3%26theater&width=500&show_text=true&height=439&appId" width="400" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
</div>
<!-- Add -->

</div>
</aside>
<!-- Aside -->

</div>
</div>
</section>
<!-- Blog Detail -->

</main>
<!-- Main -->    
@endsection
