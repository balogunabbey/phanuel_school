@extends('layouts.app')
@section('title', 'Contact Us')
@section('content')

<!-- Inner Banner -->
<div class="inner-banner team text-center" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-08.jpg') }}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>We’d love to hear from you</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

    <!-- Contact -->
    <section class="contact-holder gray-bg tc-padding-bottom">
        <div class="container">
            <div class="row">

                <!-- Content -->
                <div class="col-sm-8">
                    <div class="content">

                        <!-- Form Heading -->
                        <div class="form-heading">
                            <h5 style="color: #676e78;">Contact us / Arrange a visit</h5>
                        </div>
                        <!-- Form Heading -->


                        <!-- Form -->
                        <form class="comment-form contact" action="{{ route('feedback.page') }}" method="post"
                            data-parsley-validate enctype="multipart/form-data">

                            @csrf

                            <div class="form-group" id="full-name-group">
                                <label>Your Name <br>
                                    @if ($errors->has('your_name'))
                                    <span style="color:red; font-size:10px; text-transform: capitalize;">{{ $errors->first('your_name') }}</span>
                                    @endif
                                </label>
                                <input type="text" data-parsley-required class="form-control" placeholder="Your Full Name" name="your_name">
                                <i class="icon-user-circle-o"></i>
                             </div>


                            <div class="form-group" id="email-group">
                                <label>Your Email <br>
                                     @if ($errors->has('your_email'))
                                    <span style="color:red; font-size:10px; text-transform: capitalize;">{{ $errors->first('your_email') }}</span>
                                    @endif
                                </label>

                                <input type="text" data-parsley-type="email" data-parsley-required class="form-control" placeholder="email@example.com" name="your_email">
                                <i class="icon-envelope-o"></i>
                            </div>


                            <div class="form-group" id="phone-group">
                                <label>Phone Number <br>
                                    @if ($errors->has('your_phone'))
                                    <span style="color:red; font-size:10px; text-transform: capitalize;">{{ $errors->first('your_phone') }}</span>
                                    @endif
                                </label>
                            <input type="text" data-parsley-type="digits" class="form-control" placeholder="12345678910" name="your_phone">
                                <i class="icon-phone2"></i>
                            </div>


                            <div class="form-group" id="message-group">
                                <textarea class="form-control" data-parsley-length="[6, 10]" rows="10" placeholder="Enter Message" name="your_message"></textarea>
                            </div>
                            @if ($errors->has('your_message'))
                                    <span class="m-2" style="color:red; font-size:10px; text-transform: capitalize; margin:5px !important;">{{ $errors->first('your_message') }}</span>
                                    @endif
                    <button class="tc-btn full-with shadow-0">Submit Message</button>
                        </form>
                        <!-- Form -->

                    </div>
                </div>
                <!-- Content -->

                <!-- Aside -->
                <aside class="col-sm-4">
                    <div class="address-figures tc-padding-top">
                        <ul class="address-list style-2">
                            <li>
                                <span>Conatct Us</span>
                                <p>72 Ijede road, Idi -roko Bus-stop
                                    Itamaga, Ikorudu, Lagos State.</p>
                            </li>
                            <li>
                                <span>For Admissions</span>
                                <p>admission@phanuelschools.sch.ng</p>
                            </li>
                            <li>
                                <span>Email Address</span>
                                <p>hello@phanuelschools.sch.ng</p>
                            </li>
                            <li>
                                <span>For Other Queries</span>
                                <p>hello@phanuelschools.sch.ng</p>
                            </li>
                        </ul>
                        <div class="tc-social-icons">
                            <ul>
                                <li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
                                <li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
                                <li><a class="google-plus" href="#"><i class="icon-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </aside>
                <!-- Aside -->

            </div>
        </div>
    </section>
    <!-- Contact -->

    <!-- Contact Paralax -->
    {{-- <div class="has-layout"><img src="{{ asset('static/images/contact-paralax.jpg') }}" alt=""></div> --}}

    <!-- Contact Paralax -->

</main>
<!-- Main -->
@endsection