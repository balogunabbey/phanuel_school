@extends('layouts.app')
@section('title', 'Parents Teachers Association')
@section('content')
<!-- Inner Banner -->
<div class="inner-banner team" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-02.jpg') }}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>THE PARENTS/TEACHERS’ ASSOCIATION</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->


<!-- Main -->
<main id="main">

    <section class="blogs-holder style-2 tc-padding-bottom">
        <div class="container">

            <!-- Content -->
            <div class="content has-layout">

                <!-- Breadcrumbs -->
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="{{ route('about.page') }}"><i class="icon-folder"></i> About Us</a></li>
                        <li>Parents Teacher's Association</li>
                        <li><a href="{{ route('home.page') }}"><i class="icon-home22"></i> Back to Home</a></li>
                    </ul>
                </div>
                <!-- Breadcrumbs -->

                <div class="single-blog-detail">
                    <div class="blog-article">

                        <p style="font-style: justify; font-size:16px;">The Parents/ Teachers’ Association is a formal
                            organization
                            whose membership is made up of parents, teachers and staff of Phanuel Schools. It is
                            intended to facilitate parental participation in the school. It seeks to advance education
                            by encouraging the fullest co-operation between the home and the school. Every parent whose
                            child is admitted as a student in Phanuel Schools automatically becomes a member.
                            The Association creates a platform for parents, teachers and staff to discuss the growth of
                            the school, especially as it affects the progress of the pupils and students.</p>


                        <p style="font-style: justify;">The PTA has executive members who implement the decisions and
                            goals of the Association. Membership of the Executive Committee is determined by periodic
                            elections. Parents who are active in the PTA have the chance of vying for elective positions
                            in the Executive Committee.</p>
                    </div>
                    <!-- Content -->

                </div>
    </section>


</main>
<!-- Main -->

@endsection