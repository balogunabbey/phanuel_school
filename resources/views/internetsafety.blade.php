@extends('layouts.app')
@section('title', 'Internet Safety')
@section('content')
<!-- Inner Banner -->
<div class="inner-banner team text-center" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-07.jpg') }}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>Internet Safety</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

    <!-- Gallery Holder -->
    <section class="gallery-holder tc-padding-bottom gray-bg">
        <div class="container">
            <div class="content has-layout">

                <!-- Breadcrumbs -->
                <div class="breadcrumbs">
                    <ul>
                        <li><i class="icon-folder"></i> Parents</li>
                        <li>Internet Safety</li>
                        <li><a href="{{ route('home.page') }}"><i class="icon-home22"></i> Back to Home</a></li>
                    </ul>
                </div>
                <!-- Breadcrumbs -->

                <div class="gallery">

                    <div class="gallery-panel has-layout">
                        <div class="tab-pane active" id="tab1">

                            <!-- Main Heading -->
                            <div class="main-heading-holder">
                                <div class="main-heading">
                                    <h2 class="color-3">Concerned about who your child might be talking to online? The
                                        PDF should provide valuable safety tips for you to keep your child safe when
                                        surfing the net.</h2>
                                    <p>&nbsp;</p>
                                    <p><a href='../doc/internet-safety.pdf'><img
                                                src="https://image.flaticon.com/icons/svg/179/179483.svg" width="50"
                                                height="50" alt="" />Click here to download PDF</a></p>
                                    <p>&nbsp;</p>
                                    <a target="_blank" class="tc-btn shadow-0" href="http://www.childnet.com/">Learn
                                        More</a>
                                </div>
                            </div>
                            <!-- Main Heading -->

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- Content -->

</main>
<!-- Main -->
@endsection