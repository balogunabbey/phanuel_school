@extends('layouts.app')
@section('title', 'Admission')
@section('content')

<!-- Inner Banner -->
<div class="inner-banner gallery text-center" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-04.jpg') }}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>Admisison</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

    <!-- Gallery Holder -->
    <section class="gallery-holder tc-padding-bottom gray-bg">
        <div class="container">
            <div class="content has-layout">

                <!-- Breadcrumbs -->
                <div class="breadcrumbs">
                    <ul>
                        <li><i class="icon-folder"></i> Academic</li>
                        <li>Admission</li>
                        <li><a href="{{ route('home.page') }}"><i class="icon-home22"></i> Back to Home</a></li>
                    </ul>
                </div>
                <!-- Breadcrumbs -->


                <div class="gallery">


                    <div class="gallery-panel has-layout">
                        <div class="tab-pane active">

                            <div class="main-heading-holder">
                                <div class="main-heading">


                                    <div class="school-fecilities dot-heading has-layout">
                                        <div class="see-also dot-heading has-layout">
                                            <h3 class="text-left">Admissions for Nursery School</h3>
                                            <p style="text-align:justify; margin-bottom:25px;">Every student is expected
                                                to fill an
                                                admission form. The form is
                                                obtainable at
                                                the school or downloadable on our website. The form must be duly
                                                completed and
                                                returned with the required documents listed on the form including
                                                immunization
                                                records. The Admissions Officer would review the documents and a Letter
                                                of
                                                admission would then be written to the Parents. All fees must be paid
                                                before the
                                                pupil starts school.</p>


                                            <h3 class="mt-2 text-left">Admissions for Primary School (Reception to Year
                                                6)</h3>
                                            <h4 class="text-left">1. Examination Form</h4>
                                            <p style="text-align:justify; margin-bottom:25px;">Every candidate is
                                                expected to fill an
                                                examination form. The form is
                                                obtainable at the school or downloadable from our website and an
                                                examination fee is payable at this point for all candidates. The form
                                                must be duly completed and returned with the required documents listed
                                                on the form at least 1 week before the examination date.
                                            </p>

                                            <h4 class="text-left">2. Examinations</h4>
                                            <p style="text-align:justify; margin-bottom:25px;">The entrance examinations
                                                must be scheduled.
                                                In each
                                                age-group there are
                                                papers in English and Mathematics.
                                            </p>

                                            <h4 class="text-left">3. Reports</h4>
                                            <p style="text-align:justify; margin-bottom:25px;">Copies of the last three
                                                term’s report for
                                                each candidate must be
                                                submitted along with the forms and should be duly signed and stamped by
                                                the Head teacher of each candidate’s school.
                                            </p>

                                            <h4 class="text-left">4. Offer of Places</h4>
                                            <p style="text-align:justify; margin-bottom:25px;">In awarding places, the
                                                Head of school will
                                                take into account the
                                                information gained from the Examination and the report, however
                                                performance on the entrance examination is the deciding factor.
                                                Following this examination, where required, pupils will be offered
                                                conditional places. Close attention is also given to the reference from
                                                the pupils’ primary school heads. In order to secure a place, a N70, 000
                                                holding fee must be paid prior to admittance. This fee is a NON
                                                REFUNDABLE deposit but it is counted as part of the first term fees. All
                                                fees must be paid in full before the pupil starts school.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Main Heading -->
                            <div class="teacher-quotes style-2 has-layout">
                                <div class="quotes">
                                    <p>We have well-packaged extra-curricular activities to complement and enhance
                                        classroom
                                        experiences and provide pupils/students with opportunities for personal growth
                                        and
                                        enrichment. Phanuel Schools have what you are looking for.</p>
                                    <ul class="share-btn btn-list">
                                        <li><a class="tc-btn shadow-0 facebook"
                                                href="{{ route('admission.enrol')}}">Register Now</a>
                                        </li>
                                    </ul>
                                </div>
                                <img class="girl-layer" src="{{ asset('static/images/slant.png')}}" alt="Enrol Now"
                                    height="400">
                            </div>


                        </div>
                    </div>


                </div>


            </div>
        </div>
    </section>


</main>
<!-- Main -->
@endsection