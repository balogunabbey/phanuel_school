@extends('layouts.app')
@section('title', 'Creche') 
@section('content')

<!-- Inner Banner -->
<div class="inner-banner team" data-enllax-ratio="-.3" style="background: url({{ asset('static/images/inner-banners/img-05.jpg') }}) 50% 0% no-repeat fixed;">
<div class="container">
<div class="inner-heading">
<h2>Nursery</h2>
</div>
</div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

<!-- Blog Detail -->
<section class="blogs-holder style-2 tc-padding-bottom">
<div class="container">

<!-- Content -->
<div class="content has-layout">

<!-- Breadcrumbs -->
<div class="breadcrumbs">
<ul>
<li><i class="icon-home22"></i>Home Page</li>
<li>Classes</li>
<li>Creche</li>
{{-- <li><a href="../">Back to Home</a></li> --}}
</ul>
</div>
<!-- Breadcrumbs -->

<div class="single-blog-detail">
<div class="single-blog-title">
<h2>Creche</h2>
</div>

<div class="blog-article">
<p>The aim of the school is to provide a broad and balanced curriculum which will enable children to develop intellectually, aesthetically, physically, socially, morally and spiritually.</p>

<p>The school aims to provide first-hand learning experiences in which children are self-motivated and interested, through which they will acquire basic skills and each develop to their full potential. Particular emphasis is placed on children acquiring literacy and numeracy. A daily literacy and numeracy session takes place in each class. The children's work is planned so that they may proceed at their own rate within their level of ability. On some occasions children gather as a class, or in groups, and on other occasions an individual approach will be taken.</p>
</div>


<!-- Content -->

</div>
</section>


</main>
<!-- Main -->
@endsection
