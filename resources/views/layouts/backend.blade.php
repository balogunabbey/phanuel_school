@include('components.head')
<body>
    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <div id="wrapper" class="wrapper bg-ash">
@include('components.header')
 <!-- Page Area Start Here -->
 <div class="dashboard-page-one">
 @include('components.sidebar')

@yield('content')

    </div>
    <!-- Page Area End Here -->
</div>


@include('components.footer')
