    <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Twitter -->
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">

    <!-- Facebook -->
    <meta property="og:url" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">

    <meta property="og:image" content="">
    <meta property="og:image:secure_url" content="">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title') - phanuel Schools</title>
    <style>
    .tx-danger {
        color: #f00;
    }
    .tx-warning {
        color: #f6b61d;
    }
    </style>

    <!-- Fonts -->


    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('static/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{ asset('static/css/icomoon.css')}}" rel="stylesheet">
    <link href="{{ asset('static/css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('static/style.css')}}" rel="stylesheet">
    <link href="{{ asset('static/css/main.css')}}" rel="stylesheet">
    <link href="{{ asset('static/css/responsive.css')}}" rel="stylesheet">
    <link href="{{ asset('static/css/transition.css')}}" rel="stylesheet">    
    <link href="{{ asset('static/css/parsley.css')}}" rel="stylesheet">    
    <!-- Date Picker CSS -->
    <link href="{{ asset('static/auth/css/datepicker.min.css')}}" rel="stylesheet">    
    <!-- Toast CSS -->
    <link href="{{ asset('static/css/toastr.min.css')}}" rel="stylesheet">   
    <!-- JavaScripts -->
    <script src="{{ asset('static/scripts/modernizr.js')}}"></script>
    </head>
<body>

<!-- Wrapper -->
<div class="wrapper push">

<!-- Header -->
<header id="header" class="header">

<!-- Top Bar -->
<div class="top-bar">
<div class="container">

<!-- News -->
<div class="announcements">
<p><span>Motto:</span>Early development is fundamental to the growth of your child.</p>
</div>
<!-- News -->

<!-- Contact -->
<div class="topbar-address">
<a href="#" class="language-drop">
<img src="{{asset('static/images/flag.png')}}" alt="">
</a>
<a class="animate pulse infinite addmissions-open tx-warning" href="#"><i class="icon-star-full"></i>Admissions Open</a>
<p><span>Contact Us</span>+234 802 849 5250</p>
</div>
<!-- Contact -->

</div>
</div>
<!-- Top Bar -->

<!-- Nav Holder -->
<div class="nav-holder">
<div class="container">
<div class="p-relative has-layout">

<!-- Logo -->
<div class="logo">
<a href="{{ route('home.page') }}"><img src="{{asset('static/images/logo.jpg')}}" alt="Phanuel schools"></a>
</div>
<!-- Logo -->
{{-- {{ route('routeName') }} --}}

<!-- Navigation -->
<nav class="navigation">
<a class="toggleMenu" href="#menu"><i class="icon-navicon"></i></a>
<ul class="nav-list">

<li>
<a class="color-2" href="#">Discover</a>
<ul>
<li><a href="{{ route('about.page') }}">About Us</a></li>
<li><a href="{{ route('pta.page') }}">PTA</a></li>
<li><a href="{{ route('vacancy.page') }}">Career With Us</a></li>
</ul>
</li>	

<li><a class="color-1" href="#">Admissions</a>
    <ul>
    <li><a href="{{ route('admission.page') }}">Currently Enrolling</a></li>
    <li><a href="{{ route('admission.enrol')}}">Admission Form</a></li>
    </ul>
    </li>

<li><a class="color-4" href="{{ route ('classes.page') }}">Classes</a></li>


<li><a class="color-3" href="#">Pupils</a>
<ul>
<li><a href="{{ route('curriculum.page') }}">Curriculum</a></li>
<li><a href="{{ route('kidszone.page') }}">Kids Zone</a></li>
</ul>
</li>

<li><a class="color-1" href="{{ route ('staffs.page') }}">Teachers</a></li>


<li>
<a class="color-5" href="#">Parents</a>
<ul>
<li><a href="{{ route('termsDate.page') }}">Term Dates</a></li>
<li><a href="{{ route('internetSafety.page') }}">Internet Safety</a></li>
<li><a href="{{ route('absences.page') }}">Absences</a></li>
<li><a href="{{ route('testimony.page') }}">Share Your Story</a></li>
</ul>
</li>

<li>
<a class="color-6" href="#">Media</a>
<ul>
<li><a href="{{ route('gallery.page') }}">Gallery</a></li>
<li><a href="#">Newsletter</a></li>
</ul>
</li>

<li><a class="color-7" href="{{ route('contactus.page') }}">Contact</a></li> 

</ul>
</nav>
<!-- Navigation -->

<!-- Search Bar -->
<div class="search-bar">
<div class="form-group">
<input type="search" class="form-control" name="search" placeholder="Enter any Keyword">
<i class="icon-search"></i>
</div>
<a class="tc-btn" href="">Search</a>
<span class="search-lable"><i class="icon-search"></i></span>
</div>
<!-- Search Bar -->

<!-- Search Open Btn -->
<span id="search-open-btn" class="search-btn"><i class="icon-search"></i></span>
<!-- Search Open Btn -->

</div>
</div>
</div>
<!-- Nav Holder -->

</header>
<!-- Header -->

 @yield('content')
 
<!-- Footer -->
<footer id="footer" class="footer has-layout">
<div class="footer-bg">
<div class="container">

<!-- Footer Widgets -->
<div class="row">

<!-- Footer Widget -->
<div class="col-lg-4 col-md-4 col-xs-6 r-full-width">
<div class="footer-widget">
<h4>Location</h4>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d990.7970319799936!2d3.555006429163572!3d6.623538699700659!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103bef98fa58b6bd%3A0xff82e470f0e9eca8!2sIjede+Rd%2C+Ikorodu!5e0!3m2!1sen!2sng!4v1512923556748" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
</div>
<!-- Footer Widget -->

<!-- Footer Widget -->
<div class="col-lg-4 col-md-4 col-xs-6 r-full-width">
<div class="footer-widget">
<h4>Contact Us</h4>
<ul class="address-list">
<li>
<div><i class="icon-map-o"></i>72 Ijede road, Idi -Iroko Bus-stop Itamaga, Ikorudu, Lagos State.</div>
</li>
<li>
<div><i class="icon-phone"></i>Phone: 0802 849 5250</div>
</li>
<li>
<div><i class="icon-envelope-o"></i>Email: hello@phanuelschools.sch.ng</div>
</li>
<li>
<div><i class="icon-clock"></i>Work Hours: Monday - Friday: 08:00AM - 04:00PM</div>
</li>
</ul>
</div>
</div>
<!-- Footer Widget -->

<!-- Footer Widget -->
<div class="col-sm-4 hidden-sm hidden-xs">
<div class="footer-widget">
<h4>Quick Links</h4>
<div class="quick-links-holder">
<div class="quick-links mb-50">
<h5>For Pupils</h5>
<ul>
<li><a href="{{ route('curriculum.page') }}">Curriculum</a></li>
<li><a href="{{ route('kidszone.page') }}">Kids Zone</a></li>
</ul>
</div>
<div class="quick-links mb-50">
<h5>For Parents</h5>
<ul>
<li><a href="{{ route('termsDate.page') }}">Term Dates</a></li>
<li><a href="{{ route('absences.page') }}">Absences</a></li>
</ul>
</div>
<div class="quick-links">
<h5>Media Center</h5>
<ul>
<li><a href="{{ route('gallery.page') }}">Our Gallery</a></li>
<li><a href="{{ route('news.page') }}">News and Events</a></li>
</ul>
</div>

<div class="quick-links">
<h5>SITE INFORMATION</h5>
<ul>
<li><a href="{{ route('internetSafety.page') }}">Internet Safety</a></li>
<li><a href="{{ route('privacy.page') }}">Privacy Policy</a></li>
</ul>
</div>

</div>
</div>
</div>
<!-- Footer Widget -->

</div>
<!-- Footer Widgets -->

<!-- Sub Footer -->
<div class="sub-footer">
<div class="tc-social-icons">
<ul>
<li><a class="facebook" href=""><i class="icon-facebook"></i></a></li>
<li><a class="google-plus" href=""><i class="icon-instagram"></i></a></li>
<li><a class="twitter" href=""><i class="icon-twitter"></i></a></li>
</ul>
</div>
<p>&copy;Copyright 20<?php echo date("y");?> Phanuel Schools. All Right Reserved.</p>
</div>
<!-- Sub Footer -->

</div>
</div>
</footer>
<!-- Footer -->

</div>
<!-- Wrapper -->


<!-- Slide menu -->
<nav id="menu" class="slide-menu">
<a class="slide-logo" href=""><img src="{{ asset('static/images/logo.jpg') }}" alt="Phanuel Schools"></a>
<ul>


<li>
<a data-toggle="collapse" href="#collapse-1" class="color-2 drop-icon">school</a>
<ul id="collapse-1" class="panel-collapse collapse">
<li><a href="{{ route('about.page') }}">About Us</a></li>
 <li><a href="{{ route ('admission.page') }}">Admissions</a></li>
<li><a href="{{ route('pta.page') }}">PTA</a></li>
<li><a href="{{ route('vacancy.page') }}">Career With Us</a></li>
</ul>
</li>


<li><a class="color-4" href="{{ route ('classes.page') }}">Classes</a></li>


<li>
<a data-toggle="collapse" href="#collapse-2" class="color-3 drop-icon">Pupils</a>
<ul id="collapse-2" class="panel-collapse collapse">
<li><a href="{{ route('curriculum.page') }}">Curriculum</a></li>
<li><a href="{{ route('kidszone.page') }}">Kids Zone</a></li>
</ul>
</li>

<li><a class="color-1" href="{{ route ('staffs.page') }}">Teachers</a></li>


<li>
<a data-toggle="collapse" href="#collapse-3" class="color-5 drop-icon">Parents</a>
<ul id="collapse-3" class="panel-collapse collapse">
<li><a href="{{ route('termsDate.page') }}">Term Dates</a></li>
<li><a href="{{ route('internetSafety.page') }}">Internet Safety</a></li>
<li><a href="{{ route('absences.page') }}">Absences</a></li>
<li><a href="{{ route('testimony.page') }}">Share Your Story</a></li>
</ul>
</li> 

<li>
<a data-toggle="collapse" href="#collapse-4" class="color-6 drop-icon">Media</a>
<ul id="collapse-4" class="panel-collapse collapse">
<li><a href="{{ route('gallery.page') }}">Gallery</a></li>
<li><a href="{{ route('news.page') }}">News &amp; Events</a></li>
</ul>
</li>


<li><a class="color-7" href="{{ route('contactus.page') }}">Contact</a></li> 

</ul>
</nav>
<!-- Slide menu -->

<!-- back To Button -->
<span id="scrollup" class="scrollup"><img src="{{ asset('static/images/superman.png') }}" alt=""></span>
<!-- back To Button -->

<!-- back To Button -->
<span id="scrollup" class="scrollup"><img src="{{ asset('static/images/superman.png') }}" alt=""></span>
<!-- back To Button -->

<!-- Java Script -->
<script src="{{ asset('static/scripts/jquery.js') }}"></script>
<script src="{{ asset('static/scripts/bootstrap.js') }}"></script>
     <!-- Toast js -->
<script src="{{ asset('static/scripts/toastr.min.js') }}"></script>
{{--         <script>
            $(".alert.flash").fadeTo(2000,500).slideUp(500,function(){
                $(".alert.flash").slideUp(500);
            })
        </script>  --}}
<script src="{{ asset('static/scripts/sliderPro.js') }}"></script>
<script src="{{ asset('static/scripts/bigSlide.js') }}"></script>
<script src="{{ asset('static/scripts/slick.js') }}"></script>
<script src="{{ asset('static/scripts/parallax.js') }}"></script>
<script src="{{ asset('static/scripts/countdown.js') }}"></script>
<script src="{{ asset('static/scripts/countTo.js') }}"></script>
<script src="{{ asset('static/scripts/spinner.js') }}"></script> 
<script src="{{ asset('static/scripts/bootstrap-select.js') }}"></script> 
<script src="{{ asset('static/scripts/star-rating.js') }}"></script> 
<script src="{{ asset('static/scripts/appear.js') }}"></script>
<script src="{{ asset('static/scripts/prettyPhoto.js') }}"></script>
<script src="{{ asset('static/scripts/isotope.pkgd.js') }}"></script>
<script src="{{ asset('static/scripts/wow-min.js') }}"></script>
<!-- Put all Functions in functions.js -->
<script src="{{ asset('static/scripts/functions.js') }}"></script>
<script src="{{ asset('static/scripts/parsley.js') }}"></script>
<!-- Date Picker Js -->
<script src="{{ asset('static/auth/js/datepicker.min.js') }}"></script>

@include('alert.alert') 


</body>
</html>



