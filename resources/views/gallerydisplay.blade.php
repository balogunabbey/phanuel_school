@extends('layouts.app')
@section('title', 'Gallery Preview')
@section('content')

<!-- Inner Banner -->
<div class="inner-banner gallery text-center" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-03.jpg')}}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>School Galleries</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

    <!-- Gallery Holder -->
    <section class="gallery-holder tc-padding-bottom gray-bg">
        <div class="container">
            <div class="content has-layout">

                <!-- Breadcrumbs -->
                <div class="breadcrumbs">
                    <ul>
                        <li><i class="icon-folder"></i> Gallery</li>
                        <li>Carol Night 2017</li>
                        <li><a href="{{ route('home.page')}}"><i class="icon-home22"></i> Back to Home</a></li>
                    </ul>
                </div>
                <!-- Breadcrumbs -->

                <!-- Gallery -->
                <div class="gallery">

                    <!-- Gallery Tabs Panels -->
                    <div class="gallery-panel has-layout">
                        <div class="tab-pane active" id="tab1">

                            <!-- Main Heading -->
                            <div class="main-heading-holder">
                                <div class="main-heading">
                                    <h2>Carol Night 2017</h2>
                                </div>
                            </div>
                            <!-- Main Heading -->

                            <!-- Thumbnail -->
                            <ul id="gallery-slides" class="gallery-slides">


                                <!-- gallery Figure -->
                                <li>
                                    <img src="{{ asset('static/images/1.jpg') }}" alt="Carol Nights 2017">
                                    <span>Carol Night 2017</span>
                                </li>
                                <!-- gallery Figure -->

                                <!-- gallery Figure -->
                                <li>
                                    <img src="{{ asset('static/images/2.jpg') }}" alt="Carol Nights 2018">
                                    <span>Carol Night 2017</span>
                                </li>
                                <!-- gallery Figure -->

                                <!-- gallery Figure -->
                                <li>
                                    <img src="{{ asset('static/images/3.jpg') }}" alt="Carol Nights 2019">
                                    <span>Carol Night 2017</span>
                                </li>
                                <!-- gallery Figure -->

                            </ul>
                            <ul id="gallery-thumnail" class="gallery-thumnail">
                                <!-- gallery Figure -->
                                <li><img src="{{ asset('static/images/1.jpg') }}" alt="Carol Nights 2017"></li>
                                <li><img src="{{ asset('static/images/2.jpg') }}" alt="Carol Nights 2018"></li>
                                <li><img src="{{ asset('static/images/3.jpg') }}" alt="Carol Nights 2019"></li>
                                <!-- gallery Figure -->


                            </ul>
                            <!-- Thumbnail -->

                            <!-- social Icons -->
                            <div class="btn-nd-shares has-layout">
                                <ul class="share-btn btn-list">
                                    <li><a class="tc-btn shadow-0 facebook" href="#"><i
                                                class="icon-facebook-official"></i>Share on Facebook</a></li>
                                    <li><a class="tc-btn shadow-0 twitter" href="#"><i class="icon-twitter"></i>Share on
                                            Twiiter</a></li>
                                    {{-- <li><a class="tc-btn shadow-0 plus" href="#">+</a></li> --}}
                                </ul>
                            </div>
                            <!-- social Icons -->

                        </div>
                    </div>
                    <!-- Gallery Tabs Panels -->

                </div>
                <!-- Gallery -->

            </div>
        </div>
    </section>
    <!-- Gallery Holder -->

</main>
<!-- Main -->
@endsection