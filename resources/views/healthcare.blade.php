@extends('layouts.app')
@section('title', 'Health Care')
@section('content')

<!-- Inner Banner -->
<div class="inner-banner gallery text-center" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-04.jpg') }}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>HEADLICE - Information from NHIS</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

    <!-- Gallery Holder -->
    <section class="gallery-holder tc-padding-bottom gray-bg">
        <div class="container">
            <div class="content has-layout">

                <!-- Breadcrumbs -->
                <div class="breadcrumbs">
                    <ul>
                        <li><i class="icon-folder"></i> Parents</li>
                        <li>Health Care</li>
                        <li><a href="{{ route('home.page') }}"><i class="icon-home22"></i> Back to Home</a></li>
                    </ul>
                </div>
                <!-- Breadcrumbs -->

                <div class="gallery">

                    <div class="gallery-panel has-layout">
                        <div class="tab-pane active" id="tab1">

                            <!-- Main Heading -->
                            <div class="main-heading-holder">
                                <!-- Nersery Statistics -->
                                <div class="statistics style-3 has-layout">
                                    <div class="main-heading-holder">
                                        <div class="main-heading">
                                            <h2>The only way to be certain that you or your child has head lice is to
                                                find a live louse.</h2>
                                            <p style="text-align: justify;">Spotting head lice in hair can be very
                                                difficult, so it's best to try to comb them out with a detection comb.
                                                Detection combs are special fine-toothed plastic combs that you can buy
                                                from your local pharmacy, supermarket or online. A comb with flat-faced
                                                teeth and a tooth spacing of 0.2-0.3mm is best. Detection combing can be
                                                carried out on dry or wet hair. Dry combing takes less time, but wet
                                                combing is more accurate because washing with conditioner stops head
                                                lice moving.</p>
                                        </div>

                                        <div class="event-lineup">
                                            <div class="event-lineup-title">
                                                <span class="tc-btn shadow-0">To use the wet detection method:</span>
                                            </div>
                                            <ul class="event-lineup-list">
                                                <li>
                                                    <h4>Wash the hair with ordinary shampoo and apply plenty of
                                                        conditioner.</h4>
                                                </li>
                                                <li>
                                                    <h4>Use an ordinary, wide-toothed comb to straighten and untangle
                                                        the hair.</h4>
                                                </li>
                                                <li>
                                                    <h4>Once the comb moves freely through the hair without dragging,
                                                        switch to the louse detection comb.</h4>
                                                </li>
                                                <li>
                                                    <h4>Make sure the teeth of the comb slot into the hair at the roots,
                                                        with the edge of the teeth lightly touching the scalp.</h4>
                                                </li>
                                                <li>
                                                    <h4>Draw the comb down from the roots to the ends of the hair with
                                                        every stroke, and check the comb for lice each time – remove
                                                        lice by wiping the comb with tissue paper or rinsing it.</h4>
                                                </li>
                                                <li>
                                                    <h4>Work through the hair, section by section, so that the whole
                                                        head of hair is combed through.</h4>
                                                </li>
                                                <li>
                                                    <h4>Do this at least twice to help ensure you haven't missed any
                                                        areas and continue until you find no more lice.</h4>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- Event Lineup -->

                                        <!-- Event Lineup -->
                                        <div class="event-lineup">
                                            <div class="event-lineup-title">
                                                <span class="tc-btn shadow-0">To use the dry detection method:</span>
                                            </div>
                                            <ul class="event-lineup-list">
                                                <li>
                                                    <h4>Use an ordinary, wide-toothed comb to straighten and untangle
                                                        the hair.</h4>
                                                </li>
                                                <li>
                                                    <h4>Once the comb moves freely through the hair without dragging,
                                                        switch to the louse detection comb.</h4>
                                                </li>
                                                <li>
                                                    <h4>Make sure the teeth of the comb slot into the hair at the roots,
                                                        with the edge of the teeth lightly touching the scalp.</h4>
                                                </li>
                                                <li>
                                                    <h4>Draw the comb down from the crown to the ends of the hair with
                                                        every stroke.</h4>
                                                </li>
                                                <li>
                                                    <h4>Look for lice as the comb is drawn through the hair. If you see
                                                        a louse, trap it against the face of the comb with your thumb to
                                                        stop if being repelled by static electricity.</h4>
                                                </li>
                                                <li>
                                                    <h4>Comb each section of hair three or four times before moving on
                                                        to the next section, until the whole head has been combed
                                                        through.</h4>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- Event Lineup -->

                                    </div>

                                </div>
                                <!-- Nersery Statistics -->
                            </div>
                            <!-- Main Heading -->


                        </div>
                    </div>

                </div>


            </div>
        </div>
    </section>
    <!-- Content -->

</main>
<!-- Main -->
@endsection