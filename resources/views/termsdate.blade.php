@extends('layouts.app')
@section('title', 'Terms &amp; Dates')
@section('content')
<!-- Inner Banner -->
<div class="inner-banner team text-center" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-07.jpg')}}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>School Term Dates</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">


    <section class="gallery-holder tc-padding-bottom gray-bg">
        <div class="container">
            <div class="content has-layout">

                <!-- Breadcrumbs -->
                <div class="breadcrumbs">
                    <ul>
                        <ul>
                            <li><i class="icon-folder"></i> Parents</li>
                            <li>School Term Dates</li>
                            <li><a href="{{ route('home.page') }}"><i class="icon-home22"></i> Back to Home</a></li>
                        </ul>
                </div>
                <!-- Breadcrumbs -->


                <div class="gallery">

                    <div class="gallery-tabs-nav has-layout">
                        <ul>
                            <li class="active">
                                <a href="{{ route('kidszone.page') }}">
                                    <figure class="gallery-figure rotate-1">
                                        <img src="{{ asset('static/images/blogs-grid/img-01.jpg') }}" alt="">
                                        <div class="overlay">
                                            <h4 class="position-center-center">Kids Zone</h4>
                                        </div>
                                    </figure>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('curriculum.page') }}">
                                    <figure class="gallery-figure">
                                        <img src="{{ asset('static/images/blogs-grid/img-02.jpg') }}" alt="">
                                        <div class="overlay">
                                            <h4 class="position-center-center">Curriculum</h4>
                                        </div>
                                    </figure>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admission.page') }}">
                                    <figure class="gallery-figure rotate-2">
                                        <img src="{{ asset('static/images/blogs-grid/img-03.jpg') }}" alt="">
                                        <div class="overlay">
                                            <h4 class="position-center-center">Admission</h4>
                                        </div>
                                    </figure>
                                </a>
                            </li>
                        </ul>
                    </div>



                    <div class="gallery-panel has-layout">
                        <div class="tab-pane active" id="tab1">

                            <div class="main-heading-holder">
                                <div class="main-heading">
                                    <h2 class="color-3">Term Dates For Our Academic Year</h2>
                                    <p>&nbsp;</p>
                                    <p><a href='#'><img src="https://image.flaticon.com/icons/svg/179/179483.svg"
                                                width="50" height="50" alt="/">Click here to download calendar as a
                                            PDF</a></p>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- content -->

</main>
<!-- Main -->
@endsection