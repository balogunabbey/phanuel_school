@extends('layouts.app')
@section('title', 'Enrol your kids')
@section('content')
<style>
    .see-also-list li {
        margin: 0 0 8px;
        font-size: 14px;
        position: relative;
        padding: 0 0 0 20px;
    }

    i.icon-calendar {
        display: block;
        position: absolute;
        top: 18px;
        right: 25px;
        color: #996459;
    }

    input.error,
    select.error {
        border: 1px solid #c70000;
    }

    label.error {
        display: block;
        position: absolute;
        top: -25px;
        right: 0;
    }

    label.error::after {
        font-family: 'icomoon';
        position: absolute;
        content: '\f00d';
        right: 31px;
        top: 40px;
        font-size: 13px;
        color: #c70000;
    }

    input.success,
    select.success {
        border: 1px solid #10b759;
    }

    label.success {
        display: block;
        position: absolute;
        top: -25px;
        right: 0;
    }

    label.success::after {
        font-family: 'icomoon';
        position: absolute;
        content: '\f00c';
        right: 31px;
        top: 40px;
        font-size: 13px;
        color: #10b759;
    }
</style>

<!-- Inner Banner -->
<div class="inner-banner gallery text-center" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-04.jpg') }}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>Registration Form</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

    <!-- Gallery Holder -->
    <section class="gallery-holder tc-padding-bottom gray-bg">
        <div class="container">
            <div class="content has-layout">

                <!-- Breadcrumbs -->
                <div class="breadcrumbs">
                    <ul>
                        <li><i class="icon-folder"></i> Admission</li>
                        <li>Registration Form</li>
                        <li><a href="{{ route('home.page') }}"><i class="icon-home22"></i> Back to Home</a></li>
                    </ul>
                </div>
                <!-- Breadcrumbs -->


                <div class="gallery">
                    <div class="gallery-panel has-layout">
                        <div class="tab-pane active">
                            <div class="main-heading-holder">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-10 col-sm-offset-2 col-md-offset-1">
                                        <form action="{{ route('enrol.submit')}}" method="post" data-parsley-validate
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="see-also dot-heading has-layout">
                                                <h3 class="text-left">Basic Information</h3>
                                            </div>
                                            <div class="row">

                                                <div class="col-xs-12 col-sm-12 col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" name="first_name" id="first_name"
                                                            class="<?php echo $errors->has('first_name') ? "form-control input-lg error" : "form-control input-lg" ?>"
                                                            placeholder="<?php echo $errors->has('first_name') ? $errors->first('first_name') : "First Name" ?>"
                                                            tabindex="1" value="{{ old('first_name') }}">
                                                        @if ($errors->has('first_name'))
                                                        <label id="first_name-error" class="error"
                                                            for="first_name"></label>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" name="last_name" id="last_name"
                                                            class="<?php echo $errors->has('last_name') ? "form-control input-lg error" : "form-control input-lg" ?>"
                                                            placeholder="<?php echo $errors->has('last_name') ? $errors->first('last_name') : "Last Name" ?>"
                                                            tabindex="2" value="{{ old('last_name') }}">
                                                        @if ($errors->has('last_name'))
                                                        <label id="last_name-error" class="error"
                                                            for="last_name"></label>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" name="dob" id="dob"
                                                            class="<?php echo $errors->has('dob') ? "form-control input-lg air-datepicker error" : "form-control input-lg air-datepicker" ?>"
                                                            data-position='bottom right'
                                                            placeholder="<?php echo $errors->has('dob') ? $errors->first('dob') : "dd/mm/yyyy" ?>"
                                                            tabindex="3" value="{{ old('dob') }}">
                                                        <i class="icon-calendar"></i>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-4">
                                                    <div class="form-group">
                                                        <select
                                                            class="<?php echo $errors->has('gender') ? "form-control error" : "form-control" ?>"
                                                            name="gender" id="gender" tabindex="4" required>
                                                            <option value=" {{ old('gender') }}"
                                                                {{ old('gender') ? "selected" : "" }}>
                                                                {{ old('gender') ? old('gender') : "Select Gender *" }}
                                                            </option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                            <option value="Others">Others</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-4">
                                                    <div class="form-group">
                                                        <select
                                                            class="<?php echo $errors->has('blood_group') ? "form-control error" : "form-control" ?>"
                                                            name="blood_group" id="blood_group" tabindex="5">
                                                            <option value=" {{ old('blood_group') }}"
                                                                {{ old('blood_group') ? "selected" : "" }}>
                                                                {{ old('blood_group') ? old('blood_group') : "Select Blood Group *" }}
                                                            </option>
                                                            <option value="A">A</option>
                                                            <option value="AB">AB</option>
                                                            <option value="B">B</option>
                                                            <option value="0">O-</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-xs-12 col-sm-12 col-md-4">
                                                    <div class="form-group">
                                                        <select
                                                            class="<?php echo $errors->has('religion') ? "form-control error" : "form-control" ?>"
                                                            name="religion" id="religion" tabindex="6">
                                                            <option value=" {{ old('religion') }}"
                                                                {{ old('religion') ? "selected" : "" }}>
                                                                {{ old('religion') ? old('religion') : "Select Religion *" }}
                                                            </option>
                                                            <option value="Islam">Islam</option>
                                                            <option value="Hindu">Hindu</option>
                                                            <option value="Christian">Christian</option>
                                                            <option value="Buddish">Buddish</option>
                                                            <option value="Others">Others</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-xs-12 col-sm-12 col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" name="prev_school_name" id="prev_school_name"
                                                            class="<?php echo $errors->has('prev_school_name') ? "form-control input-lg error" : "form-control input-lg" ?>"
                                                            placeholder="<?php echo $errors->has('prev_school_name') ? $errors->first('prev_school_name') : "Previous School Name" ?>"
                                                            tabindex="8" value="{{ old('prev_school_name') }}">
                                                        @if ($errors->has('prev_school_name'))
                                                        <label id="prev_school_name-error" class="error"
                                                            for="prev_school_name"></label>
                                                        @endif
                                                    </div>
                                                </div>



                                                <div class="col-xs-12 col-sm-12 col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" name="prev_class" id="prev_class"
                                                            class="<?php echo $errors->has('prev_class') ? "form-control input-lg error" : "form-control input-lg" ?>"
                                                            placeholder="<?php echo $errors->has('prev_class') ? $errors->first('prev_class') : "Previous Class" ?>"
                                                            tabindex="9" value="{{ old('prev_class') }}">
                                                        @if ($errors->has('prev_class'))
                                                        <label id="prev_class-error" class="error"
                                                            for="prev_class"></label>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" name="intend_class" id="intend_class"
                                                            class="<?php echo $errors->has('intend_class') ? "form-control input-lg error" : "form-control input-lg" ?>"
                                                            placeholder="<?php echo $errors->has('intend_class') ? $errors->first('intend_class') : "Intended Class" ?>"
                                                            tabindex="10" value="{{ old('intend_class') }}">
                                                        @if ($errors->has('intend_class'))
                                                        <label id="intend_class-error" class="error"
                                                            for="intend_class"></label>
                                                        @endif
                                                    </div>
                                                </div>


                                                <div class="see-also dot-heading has-layout mg-t-20">
                                                    <h3 class="text-left">Parent Information</h3>
                                                </div>

                                                <div class="row">

                                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                                        <div class="form-group">
                                                            <input type="text" name="parent_first_name"
                                                                id="parent_first_name"
                                                                class="<?php echo $errors->has('parent_first_name') ? "form-control input-lg error" : "form-control input-lg" ?>"
                                                                placeholder="<?php echo $errors->has('parent_first_name') ? $errors->first('parent_first_name') : "Parent First Name" ?>"
                                                                tabindex="11" value="{{ old('parent_first_name') }}">
                                                            @if ($errors->has('parent_first_name'))
                                                            <label id="parent_first_name-error" class="error"
                                                                for="parent_first_name"></label>
                                                            @endif
                                                        </div>
                                                    </div>


                                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                                        <div class="form-group">
                                                            <input type="text" name="parent_last_name"
                                                                id="parent_last_name"
                                                                class="<?php echo $errors->has('parent_last_name') ? "form-control input-lg error" : "form-control input-lg" ?>"
                                                                placeholder="<?php echo $errors->has('parent_last_name') ? $errors->first('parent_last_name') : "Parent Last Name" ?>"
                                                                tabindex="12" value="{{ old('parent_last_name') }}">
                                                            @if ($errors->has('parent_last_name'))
                                                            <label id="parent_last_name-error" class="error"
                                                                for="parent_last_name"></label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                                        <div class="form-group">
                                                            <input type="text" name="email_address" id="email_address"
                                                                class="<?php echo $errors->has('email_address') ? "form-control input-lg error" : "form-control input-lg" ?>"
                                                                placeholder="<?php echo $errors->has('email_address') ? $errors->first('email_address') : "Parent Email Address" ?>"
                                                                tabindex="13" value="{{ old('email_address') }}">
                                                            @if ($errors->has('email_address'))
                                                            <label id="email_address-error" class="error"
                                                                for="email_address"></label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                                        <div class="form-group">
                                                            <input type="text" name="phone_number" id="phone_number"
                                                                class="<?php echo $errors->has('phone_number') ? "form-control input-lg error" : "form-control input-lg" ?>"
                                                                placeholder="<?php echo $errors->has('phone_number') ? $errors->first('phone_number') : "Parent Phone Number" ?>"
                                                                tabindex="14" value="{{ old('phone_number') }}">
                                                            @if ($errors->has('phone_number'))
                                                            <label id="phone_number-error" class="error"
                                                                for="phone_number"></label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                                        <div class="form-group">
                                                            <input type="text" name="occupation" id="occupation"
                                                                class="<?php echo $errors->has('occupation') ? "form-control input-lg error" : "form-control input-lg" ?>"
                                                                placeholder="<?php echo $errors->has('occupation') ? $errors->first('occupation') : "Parent Occupation" ?>"
                                                                tabindex="15" value="{{ old('occupation') }}">
                                                            @if ($errors->has('occupation'))
                                                            <label id="occupation-error" class="error"
                                                                for="occupation"></label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12 col-sm-12 col-md-5">
                                                        <div class="form-group">
                                                            <input type="text" name="home_address" id="home_address"
                                                                class="<?php echo $errors->has('home_address') ? "form-control input-lg error" : "form-control input-lg" ?>"
                                                                placeholder="<?php echo $errors->has('home_address') ? $errors->first('home_address') : "Home Address" ?>"
                                                                tabindex="16" value="{{ old('home_address') }}">
                                                            @if ($errors->has('home_address'))
                                                            <label id="home_address-error" class="error"
                                                                for="home_address"></label>
                                                            @endif
                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="see-also dot-heading has-layout mg-t-20">
                                                    <h3 class="text-left">Health Information</h3>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                                        <div class="form-group">
                                                            <input type="text" name="allergy" id="allergy"
                                                                class="form-control input-lg"
                                                                placeholder="Allergy if any" tabindex="7"
                                                                value="{{ old('allergy') }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mg-t-20 text-left">
                                                    <h4>Please kindly forward the following to
                                                        <strong
                                                            class="text-color-6">registrar@phanuelschools.sch.ng</strong>
                                                        after submitting the form</h4>
                                                    <ul class="see-also-list">
                                                        <li>Photocopy of child’s birth certificate</li>
                                                        <li>Two passports photo of child</li>
                                                        <li>Photocopies of child previous school reports for the past
                                                            year.
                                                        </li>
                                                    </ul>
                                                </div>



                                                <button name="tag2" type="submit"
                                                    class="tc-btn full-with shadow-0 mg-t-30 mg-b-30">Submit
                                                    Message</button>


                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Main Heading -->
                        </div>
                    </div>


                </div>


            </div>
        </div>
    </section>


</main>
<!-- Main -->
@endsection