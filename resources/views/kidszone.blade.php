@extends('layouts.app')
@section('title', 'Kids Zone')
@section('content')

<!-- Inner Banner -->
<div class="inner-banner gallery text-center" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-05.jpg') }}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>Kids Zone</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->



<!-- Main -->
<main id="main">

    <!-- Clasess -->
    <div class="classes-list style-2 gray-bg">
        <div class="container">
            <div class="row">

                <!-- Classes Figure -->
                <div class="col-lg-4 col-sm-4 col-xs-6 r-full-width">
                    <div class="classes-column">
                        <figure class="curve-up-small bg-1">
                            <img src="{{ asset('static/images/kidzone/1.jpg') }}" alt="">
                            <div class="overlay-caption position-center-center">
                                <h2>Engish</h2>
                            </div>
                            <span class="search-lable"><i class="icon-star-full"></i></span>
                        </figure>
                        <div class="classes-detail after-clear">
                            <img class="teacher-img"
                                src="{{ asset('static/images/kidzone/icon-tutoring-english.png') }}" width="114"
                                height="114" alt="">
                        </div>
                        <div class="classes-btm">
                            <a target="_blank" class="tc-btn shadow-0"
                                href="http://learnenglishkids.britishcouncil.org/en/games/story-maker">Click Here</a>
                        </div>
                    </div>
                </div>
                <!-- Classes Figure -->

                <!-- Classes Figure -->
                <div class="col-lg-4 col-sm-4 col-xs-6 r-full-width">
                    <div class="classes-column">
                        <figure class="curve-up-small bg-1">
                            <img src="{{ asset('static/images/kidzone/2.jpg') }}" alt="">
                            <div class="overlay-caption position-center-center">
                                <h2>Maths</h2>
                            </div>
                            <span class="search-lable"><i class="icon-star-full"></i></span>
                        </figure>
                        <div class="classes-detail after-clear">
                            <img class="teacher-img" src="{{ asset('static/images/kidzone/icon_math.png') }}"
                                width="114" height="114" alt="">
                        </div>
                        <div class="classes-btm">
                            <a target="_blank" class="tc-btn shadow-0"
                                href="http://www.funbrainjr.com/games/MushroomBounce">Click Here</a>
                        </div>
                    </div>
                </div>
                <!-- Classes Figure -->

                <!-- Classes Figure -->
                <div class="col-lg-4 col-sm-4 col-xs-6 r-full-width">
                    <div class="classes-column">
                        <figure class="curve-up-small bg-1">
                            <img src="{{ asset('static/images/kidzone/3.jpg') }}" alt="">
                            <div class="overlay-caption position-center-center">
                                <h2>Science</h2>
                            </div>
                            <span class="search-lable"><i class="icon-star-full"></i></span>
                        </figure>
                        <div class="classes-detail after-clear">
                            <img class="teacher-img" src="{{ asset('static/images/kidzone/microscope-icon.png') }}"
                                width="114" height="114" alt="">
                        </div>
                        <div class="classes-btm">
                            <a target="_blank" class="tc-btn shadow-0 facebook" href="https://pbskids.org/sid/">Click
                                Here</a>
                        </div>
                    </div>
                </div>
                <!-- Classes Figure -->


            </div>
        </div>
    </div>
    <!-- Clasess -->

</main>
<!-- Main -->
@endsection