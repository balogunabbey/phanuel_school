@extends('layouts.app')
@section('title', 'Absences')
@section('content')

<!-- Inner Banner -->
<div class="inner-banner team text-center" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-07.jpg')}}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>How we respond to absences</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">


    <section class="gallery-holder tc-padding-bottom gray-bg">
        <div class="container">
            <div class="content has-layout">

                <!-- Breadcrumbs -->
                <div class="breadcrumbs">
                    <ul>
                        <li><i class="icon-folder"></i> Parents</li>
                        <li>Absences</li>
                    <li><a href="{{ route('home.page') }}"><i class="icon-home22"></i> Back to Home</a></li>
                    </ul>
                </div>
                <!-- Breadcrumbs -->

                <div class="gallery">

                    <div class="gallery-panel has-layout">
                        <div class="tab-pane active" id="tab1">

                            <!-- Main Heading -->
                            <div class="main-heading-holder">
                                <div class="main-heading">
                                    <p style="font-style: justify;">At Phanuel Schoolsl our staff work hard to monitor
                                        the attendance of the whole school. We are here to help if you have any troubles
                                        or concerns about your child's time at school, or if you are having a difficult
                                        time getting your child into school. In these circumstances, our School Home
                                        Support is available to talk to.</p>
                                    <p>By law, every child that is statutory school age must attend school five days a
                                        week unless they are unwell. If your child is unwell you must call our absence
                                        line before 8.50am and leave a message stating your child's name, class and
                                        reason for absence or contact us by email on </p>
                                    <p>&nbsp;</p>
                                    <a class="tc-btn shadow-0">Absence line: </a>
                                    <p>&nbsp;</p>

                                    <div class="school-fecilities dot-heading has-layout">
                                        <div class="see-also dot-heading has-layout">
                                            <p style="font-style: justify;">When your child takes time off from school
                                                you will need to provide evidence for the reasons why. The evidence can
                                                be:</p>
                                            <ul class="see-also-list">
                                                <li>A proof of appointment (card or letter)</li>
                                                <li> Medicine prescription label (with the date and name of your child
                                                    visible).</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <p style="font-style: justify;">A text will be sent daily if the school does not
                                        receive a message. If your child is unwell, you must call the office before 9am
                                        and report your child absent. We will then require proof of your child's absence
                                        (see above). Nursery places are in high demand, so if your child is not
                                        attending regularly and their attendance falls to 87% or below you may run the
                                        risk of having your child's nursery place withdrawn.</p>


                                </div>
                            </div>
                            <!-- Main Heading -->


                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- Content -->

</main>
<!-- Main -->
@endsection