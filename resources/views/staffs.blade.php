@extends('layouts.app')
@section('title', 'Amazing Teachers') 
@section('content')
<!-- Inner Banner -->
<div class="inner-banner team text-center" data-enllax-ratio="-.3" style="background: url({{ asset('static/images/inner-banners/img-04.jpg') }}) 50% 0% no-repeat fixed;">
<div class="container">
<div class="inner-heading">
<h2>Amazing Teachers</h2>
</div>
</div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">
<p>&nbsp;</p>


<!-- Team List View -->
<section class="tc-padding-bottom">
<div class="container">


<div class="team-figure list-view  gray-bg">
<div class="row">
<div class="col-sm-4 col-xs-5 r-full-width2">
<figure> <img src="{{ asset('static/images/team/img-01.jpg')}}" alt="Balogun Abiodun"> </figure>
</div>
<div class="col-sm-8 col-xs-7 r-full-width2">
<div class="team-detail">
<h3>Balogun Abiodun</h3>
<span>Head Teacher</span>
<p>She then continued her training and gained a degree in Early Childhood studies, followed by a PGCE in Primary Education specialising in Early Years. Miss Lucinda obtained her QTS in June 2008 whilst teaching a reception class at Eaton House, The Manor School. Miss Lucinda re-joined Young England in 2009 and became Head Teacher in September 2011.</p>
<div class="tc-social-icons">
<ul>
<li class="email">baloghun.a@webstack.ng</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<!-- List View -->




<!-- List View -->


</div>
</section>
<!-- Team List View -->

</main>
<!-- Main -->


@endsection
