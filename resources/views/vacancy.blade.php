@extends('layouts.app')
@section('title', 'Career')
@section('content')

<!-- Inner Banner -->
<div class="inner-banner team" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-02.jpg') }}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>Staff Vacancies</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

    <!-- Blog Detail -->
    <section class="blogs-holder style-2 tc-padding-bottom">
        <div class="container">

            <!-- Content -->
            <div class="content has-layout">

                <!-- Breadcrumbs -->
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="{{ route('about.page') }}"><i class="icon-folder"></i> About Us</a></li>
                        <li>Careers</li>
                        <li><a href="{{ route('home.page') }}"><i class="icon-home22"></i> Back to Home</a></li>
                    </ul>
                </div>
                <!-- Breadcrumbs -->

                <div class="single-blog-detail">
                    <div class="blog-article">

                        <p style="font-style: justify;">No current vacancies. Full details regarding vacancies would be
                            posted on this section, as the need arises. To apply
                            for positions with us, all required documents should be submitted to the Head, Human
                            Resources department. </p>

                    </div>
                    <!-- Content -->

                </div>
    </section>


</main>
<!-- Main -->


@endsection