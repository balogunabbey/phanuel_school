@extends('layouts.backend')
@section('title', 'Enrol Your child')
@section('content')
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <ul>
            <li>
                <a href="{{ route('dashboard.index')}}">Dashboard</a>
            </li>
            <li>Enrol Your Child</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Admit Form Area Start Here -->
    <div class="card height-auto">
        <div class="card-body">
            <div class="heading-layout1">
                <div class="item-title mg-t-20 mg-b-20">
                    <h3>Personal Information</h3>
                </div>
            </div>
            <form class="new-added-form">
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>First Name *</label>
                        <input type="text" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Last Name *</label>
                        <input type="text" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Gender *</label>
                        <select class="select2">
                            <option value="">Please Select Gender *</option>
                            <option value="1">Male</option>
                            <option value="2">Female</option>
                            <option value="3">Others</option>
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Date Of Birth *</label>
                        <input type="text" placeholder="dd/mm/yyyy" class="form-control air-datepicker"
                            data-position='bottom right'>
                        <i class="far fa-calendar-alt"></i>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Blood Group *</label>
                        <select class="select2">
                            <option value="">Please Select Group *</option>
                            <option value="1">A+</option>
                            <option value="2">A-</option>
                            <option value="3">B+</option>
                            <option value="3">B-</option>
                            <option value="3">O+</option>
                            <option value="3">O-</option>
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Religion *</label>
                        <select class="select2">
                            <option value="">Please Select Religion *</option>
                            <option value="1">Islam</option>
                            <option value="2">Hindu</option>
                            <option value="3">Christian</option>
                            <option value="3">Buddish</option>
                            <option value="3">Others</option>
                        </select>
                    </div>

                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Class *</label>
                        <select class="select2">
                            <option value="">Please Select Class *</option>
                            <option value="1">Play</option>
                            <option value="2">Nursery</option>
                            <option value="3">One</option>
                            <option value="3">Two</option>
                            <option value="3">Three</option>
                            <option value="3">Four</option>
                            <option value="3">Five</option>
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Session *</label>
                        <select class="select2">
                            <option value="">Please Select Section *</option>
                            <option value="1">2018 / 2019</option>
                            <option value="2">2019 / 2020</option>
                            <option value="3">2020 / 2021</option>
                        </select>
                    </div>

                    <div class="col-lg-6 col-12 form-group mg-t-30">
                        <label class="text-dark-medium">Upload Profile Photo (150px X 150px)</label>
                        <input type="file" class="form-control-file">
                    </div>
                    <div class="col-12 form-group mg-t-8">
                        <button type="submit"
                            class="btn-fill-lg shadow-violet-blue bg-mauvelous  btn-hover-bluedark">Save</button> </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Admit Form Area End Here -->

    <!-- Student Table Area Start Here -->
    <div class="card height-auto">
        <div class="card-body">
            <div class="heading-layout1">
                <div class="item-title mg-t-20 mg-b-20">
                    <h3>Recently Enroled Kids</h3>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table display data-table text-nowrap bg-true-v">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Section</th>
                            <th>Class</th>
                            <th>Date Of Birth</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td class="text-center"><img src="{{ asset('static/auth/img/figure/student6.png')}}"
                                    alt="student"></td>
                            <td>Jessia Rose</td>
                            <td>Female</td>
                            <td>2019 / 2020</td>
                            <td>Basic 4</td>
                            <td>02/05/2009</td>
                            <td class="badge badge-pill badge-danger shadow-red mg-t-8">Pending</td>
                            <td>
                                <div class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span class="flaticon-more-button-of-three-dots"></span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#"><i
                                                class="fas fa-trash text-orange-red"></i>Delete</a>
                                        <a class="dropdown-item" href="#"><i
                                                class="fas fa-pen text-orange-peel"></i>Edit</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td class="text-center"><img src="{{ asset('static/auth/img/figure/student5.png')}}"
                                    alt="student"></td>
                            <td>Stanley Rose</td>
                            <td>Male</td>
                            <td>2019 / 2020</td>
                            <td>Basic 6</td>
                            <td>02/05/2001</td>
                            <td class="badge badge-pill badge-success shadow-dark-pastel-green mg-t-8">Approved
                            </td>
                            <td>
                                <div class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span class="flaticon-more-button-of-three-dots"></span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#"><i
                                                class="fas fa-trash text-orange-red"></i>Delete</a>
                                        <a class="dropdown-item" href="#"><i
                                                class="fas fa-pen text-orange-peel"></i>Edit</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Student Table Area End Here -->


    @endsection