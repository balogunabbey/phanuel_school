@extends('layouts.backend')
@section('title', 'Parent Dashboard')
@section('content')
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <h3>Welcome Back!</h3>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Dashboard summery Start Here -->
    <div class="row">
        <div class="col-3-xxxl col-sm-4 col-12">
            <div class="dashboard-summery-one">
                <div class="row">
                    <div class="col-5">
                        <img src=" {{ asset('static/auth/images/family.png') }}" alt="" />
                    </div>
                    <div class="col-7">
                        <div class="item-content">
                            <div class="item-title" style="font-weight: 500;">Total Registered Kids</div>
                            <div class="item-number"><span class="counter text-red" data-num="4503">4,503</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3-xxxl col-sm-4 col-12">
            <div class="dashboard-summery-one">
                <div class="row">
                    <div class="col-5">
                        <img src=" {{ asset('static/auth/images/notification.png') }}" alt="" />
                    </div>
                    <div class="col-7">
                        <div class="item-content">
                            <div class="item-title" style="font-weight: 500;">Total Unread Notifications</div>
                            <div class="item-number"><span class="counter text-magenta" data-num="12">12</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-3-xxxl col-sm-4 col-12">
            <div class="dashboard-summery-one">
                <div class="row">
                    <div class="col-5">
                        <img src=" {{ asset('static/auth/images/payment.png') }}" alt="" />
                    </div>
                    <div class="col-7">
                        <div class="item-content">
                            <div class="item-title" style="font-weight: 500;">Total Pending Payments</div>
                            <div class="item-number"><span class="text-blue">N</span><span class="counter text-blue"
                                    data-num="193000">N193,000</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Dashboard summery End Here -->
    <!-- Dashboard Content Start Here -->
    <div class="row">
        <div class="col-5-xxxl col-sm-6">
            <div class="card dashboard-card-twelve">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>Recently Registered Kid(s)</h3>
                        </div>
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="notice-board.html#" role="button" data-toggle="dropdown"
                                aria-expanded="false">...</a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="{{ route('kids.index')}}"><i
                                        class="fas fa-link text-orange-peel"></i>View
                                    More</a>
                            </div>
                        </div>
                    </div>
                    <div class="kids-details-wrap">
                        <div class="row">
                            <div class="col-12-xxxl col-xl-12 col-12">
                                <div class="kids-details-box mb-5">
                                    <div class="item-img" style="border-radius: 0%;">
                                        <img src="{{ asset('static/auth/img/figure/student.png')}}" alt="kids">
                                    </div>
                                    <div class="item-content table-responsive">
                                        <table class="table text-nowrap">
                                            <tbody>
                                                <tr>
                                                    <td>Full Name:</td>
                                                    <td>Quadri Allan Balogun</td>
                                                </tr>
                                                <tr>
                                                    <td>Gender:</td>
                                                    <td>Female</td>
                                                </tr>
                                                <tr>
                                                    <td>Current Class:</td>
                                                    <td>Basic 2</td>
                                                </tr>
                                                <tr>
                                                    <td>Admission ID:</td>
                                                    <td>PH-BC-#0021</td>
                                                </tr>
                                                <tr>
                                                    <td>Admission Date:</td>
                                                    <td>7<sup>th</sup>, May 2019</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12-xxxl col-xl-12 col-12">
                                <div class="kids-details-box">
                                    <div class="item-img" style="border-radius: 0%;">
                                        <img src="{{ asset('static/auth/img/figure/student.png')}}" alt="kids">
                                    </div>
                                    <div class="item-content table-responsive">
                                        <table class="table text-nowrap">
                                            <tbody>
                                                <tr>
                                                    <td>Name:</td>
                                                    <td>Balogun Abiodun Abdullateef</td>
                                                </tr>
                                                <tr>
                                                    <td>Gender:</td>
                                                    <td>Male</td>
                                                </tr>
                                                <tr>
                                                    <td>Current Class:</td>
                                                    <td>Basic 3</td>
                                                </tr>
                                                <tr>
                                                    <td>Admission ID:</td>
                                                    <td>PH-BC-#0021</td>
                                                </tr>
                                                <tr>
                                                    <td>Admission Date:</td>
                                                    <td>7<sup>th</sup>, May 2019</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-5-xxxl col-sm-6">
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>Recent Notifications</h3>
                        </div>
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                                aria-expanded="false">...</a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="{{ route('notice.index')}}"><i
                                        class="fas fa-link text-orange-peel"></i>View
                                    More</a>
                            </div>
                        </div>
                    </div>

                    <div class="notice-board-wrap">
                        <div class="notice-list">
                            <div class="post-date bg-skyblue">16 June, 2019</div>
                            <h6 class="notice-title"><a href="notice-board.html#">Great School Great School manag
                                    mene
                                    esom
                                    text of the printing Great School manag mene esom text of the printing manag
                                    mene esom text of the printing.</a></h6>
                            <div class="entry-meta"> Jennyfar Lopez / <span>5 min ago</span></div>
                        </div>
                        <div class="notice-list">
                            <div class="post-date bg-skyblue">16 June, 2019</div>
                            <h6 class="notice-title"><a href="notice-board.html#">Great School Great School manag
                                    mene
                                    esom
                                    text of the printing Great School manag mene esom text of the printing manag
                                    mene esom text of the printing.</a></h6>
                            <div class="entry-meta"> Jennyfar Lopez / <span>5 min ago</span></div>
                        </div>

                        <div class="notice-list">
                            <div class="post-date bg-skyblue">16 June, 2019</div>
                            <h6 class="notice-title"><a href="notice-board.html#">Great School Great School manag
                                    mene
                                    esom
                                    text of the printing Great School manag mene esom text of the printing manag
                                    mene esom text of the printing.</a></h6>
                            <div class="entry-meta"> Jennyfar Lopez / <span>5 min ago</span></div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-8-xxxl col-12">
            <!-- Expanse Table Area Start Here -->
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title mg-t-20 mg-b-20">
                            <h3>School Fees History</h3>
                        </div>
                    </div>
                    <form class="mg-b-20">
                        <div class="row gutters-8">
                            <div class="col-4-xxxl col-xl-3 col-lg-4 col-12 form-group">
                                <input type="text" placeholder="Search by Session ..." class="form-control">
                            </div>
                            <div class="col-4-xxxl col-xl-3 col-lg-4 col-12 form-group">
                                <input type="text" placeholder="Search by Term" class="form-control">
                            </div>
                            <div class="col-4-xxxl col-xl-4 col-lg-4 col-12 form-group">
                                <input type="text" placeholder="Search by Name" class="form-control">
                            </div>
                            <div class="col-1-xxxl col-xl-2 col-lg-4 col-12 form-group">
                                <button type="submit"
                                    class="fw-btn-fill btn-gradient-yellow shadow-orange-peel">SEARCH</button>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table data-table text-nowrap bg-true-v">
                            <thead>
                                <tr>
                                    <th>Session</th>
                                    <th>Term</th>
                                    <th>Class</th>
                                    <th>Name</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Deadline</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2016/2017</td>
                                    <td>Second Contact</td>
                                    <td>Basic 4</td>
                                    <td>Balogun Abiodun</td>
                                    <td>$2,0000.00</td>
                                    <td class="badge badge-pill badge-success d-block mg-t-8  shadow-dark-pastel-green">
                                        Paid</td>
                                    <td>02/02/2019</td>
                                    <td>
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#"><i
                                                        class="fas fa-eye text-orange-red"></i>View Invoice</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2016/2017</td>
                                    <td>Second Contact</td>
                                    <td>Basic 4</td>
                                    <td>Balogun Abiodun</td>
                                    <td>$2,0000.00</td>
                                    <td class="badge badge-pill badge-danger d-block mg-t-8 shadow-orange-red">pending
                                    </td>
                                    <td>02/02/2019</td>
                                    <td>
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#"><i
                                                        class="fas fa-eye text-dark-pastel-green"></i>View
                                                    Invoice</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Expanse Table Area End Here -->
        </div>
    </div>

    @endsection