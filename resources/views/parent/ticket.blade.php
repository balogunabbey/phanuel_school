@extends('layouts.backend')
@section('title', 'Open Ticket')
@section('content')
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <ul>
            <li>
                <a href="{{ route('dashboard.index')}}">Dashboard</a>
            </li>
            <li>Reports / Suggestions</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Add Open Tickets Here -->
    <div class="row">
        <div class="col-xl-4">
            <div class="card">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title mg-b-20  mg-t-20">
                            <h3>Create New Ticket</h3>
                        </div>
                    </div>
                    <form class="new-added-form">
                        <div class="row">
                            <div class="col-12 form-group">
                                <label>Reports / Suggestions *</label>
                                <select class="select2">
                                    <option value="">Select Preferred Option *</option>
                                    <option value="complaints">Complaints / Reports</option>
                                    <option value="suggestions">Suggestions</option>
                                </select>
                            </div>
                            <div class="col-12 form-group">
                                <label>Subject *</label>
                                <input type="text" placeholder="" class="form-control">
                            </div>

                            <div class="col-12 form-group">
                                <label>Message *</label>
                                <textarea class="textarea form-control" name="message" id="form-message" cols="10"
                                    rows="9"></textarea>
                            </div>

                            <div class="col-12 form-group mg-t-8">
                                <button type="submit"
                                    class="btn-fill-lg text-light shadow-violet-blue bg-mauvelous btn-hover-bluedark">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-xl-8 col-12">
            <div class="card dashboard-card-eleven">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title mg-b-20 mg-t-20">
                            <h3>Ticket History</h3>
                        </div>
                    </div>
                    <div class="table-box-wrap">
                        <form class="search-form-box">
                            <div class="row gutters-8">
                                <div class="col-lg-5 col-md-3 form-group">
                                    <input type="text" placeholder="Search by Subject ..." class="form-control">
                                </div>
                                <div class="col-lg-5 col-md-3 form-group">
                                    <input type="text" placeholder="dd/mm/yyyy" class="form-control">
                                </div>
                                <div class="col-lg-2 col-md-3 form-group">
                                    <button type="submit"
                                        class="fw-btn-fill btn-gradient-yellow shadow-orange-peel">Search</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive result-table-box">
                            <table class="table display data-table text-nowrap bg-true-v">
                                <thead>
                                    <tr>
                                        <th>Ticket ID</th>
                                        <th>Subject</th>
                                        <th>Entry Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>#0021</td>
                                        <td>Biodun Allan is not impressively improving</td>
                                        <td>22/02/2019</td>
                                        <td
                                            class="badge badge-pill badge-success d-block shadow-dark-pastel-green mg-t-8">
                                            Read</td>
                                        <td>
                                            <div class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                    aria-expanded="false">
                                                    <span class="flaticon-more-button-of-three-dots"></span>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#"><i
                                                            class="fas fa-trash text-orange-red"></i>Delete</a>
                                                    <a class="dropdown-item" href="#"><i
                                                            class="fas fa-eye text-dark-pastel-green"></i>View</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Add Open Tickets Here -->



    @endsection