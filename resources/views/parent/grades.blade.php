@extends('layouts.backend')
@section('title', 'Result Checker')
@section('content')
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <ul>
            <li>
                <a href="{{ route('dashboard.index')}}">Dashboard</a>
            </li>
            <li>Result Checker</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Student Attendence Search Area Start Here -->
    <div class="card height-auto">
        <div class="card-body">
            <div class="heading-layout1">
                <div class="item-title mg-b-20 mg-t-20">
                    <h3>Search Child Result</h3>
                </div>
            </div>
            <form class="new-added-form" action="{{ route('grade.summary')}}">
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Select Preferred Pupil</label>
                        <select class="select2">
                            <option value="">Select Pupil</option>
                            <option value="1">Nursery</option>
                            <option value="2">Play</option>
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Select Preferred Session</label>
                        <select class="select2">
                            <option value="0">Select Session</option>
                            <option value="1">2016-2017</option>
                            <option value="2">2017-20108</option>
                            <option value="3">2018-2019</option>
                            <option value="4">2020-2021</option>
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Select Preferred Class</label>
                        <select class="select2">
                            <option value="0">Select Month</option>
                            <option value="1">January</option>
                            <option value="2">February</option>
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Select Term</label>
                        <select class="select2">
                            <option value="0">Select Session</option>
                            <option value="1">First Term</option>
                            <option value="2">Second Term</option>
                            <option value="3">Third Term</option>
                        </select>
                    </div>
                    <div class="col-12 form-group mg-t-8">
                        <button type="submit"
                            class="btn-fill-lg shadow-violet-blue bg-mauvelous  btn-hover-bluedark">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Student Attendence Search Area End Here -->
    @endsection