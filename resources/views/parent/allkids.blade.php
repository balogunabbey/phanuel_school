@extends('layouts.backend')
@section('title', 'All Registered Kids')
@section('content')
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <ul>
            <li>
                <a href="{{ route('dashboard.index')}}">Dashboard</a>
            </li>
            <li>All Registered Ward(s)</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <div class="row">

        <div class="col-4-xxxl col-xl-5">
            <div class="card account-settings-box height-auto">
                <div class="card-body">
                    <div class="heading-layout1 mg-b-20">
                        <div class="item-title mg-b-20 mg-t-20">
                            <h3>All User</h3>
                        </div>
                    </div>
                    <div class="all-user-box">
                        <div class="media media-none--xs active">
                            <div class="item-img rounded">
                                <img src="{{ asset('static/auth/img/figure/user1.jpg')}}" class="media-img-auto"
                                    alt="user">
                            </div>
                            <div class="media-body space-md">
                                <h5 class="item-title">Balogun Abiodun</h5>
                                <div class="item-subtitle">Baisc 2</div>
                            </div>
                        </div>
                        <div class="media media-none--xs">
                            <div class="item-img rounded">
                                <img src="{{ asset('static/auth/img/figure/user2.jpg')}}" class="media-img-auto"
                                    alt="user">
                            </div>
                            <div class="media-body space-md">
                                <h5 class="item-title">Jafar Sabo</h5>
                                <div class="item-subtitle">Basic 6</div>
                            </div>
                        </div>
                        <div class="media media-none--xs">
                            <div class="item-img rounded">
                                <img src="{{ asset('static/auth/img/figure/user3.jpg')}}" class="media-img-auto"
                                    alt="user">
                            </div>
                            <div class="media-body space-md">
                                <h5 class="item-title">Fatima Obisesan</h5>
                                <div class="item-subtitle">Basic 5</div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-8-xxxl col-xl-7">
            <div class="card account-settings-box">
                <div class="card-body">
                    <div class="heading-layout1 mg-b-20">
                        <div class="item-title">
                            <h3>Kids Information</h3>
                        </div>
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                                aria-expanded="false">...</a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="fas fa-trash text-orange-red"></i>Delete</a>
                                <a class="dropdown-item"><i class="fas fa-file-alt text-dark-pastel-green"></i>Academic
                                    Records</a>
                            </div>
                        </div>
                    </div>
                    <div class="user-details-box">
                        <div class="item-img text-center">
                            <img class="rounded" src="{{ asset('static/auth/img/figure/user.jpg')}}" alt="Profile Info">
                        </div>
                        <div class="item-content">
                            <div class="info-table table-responsive" style="letter-spacing: 0.3px;
                            line-height: 20px;">
                                <table class="table text-nowrap">
                                    <tbody>
                                        <tr>
                                            <td>Full Name:</td>
                                            <td class="font-medium text-dark-medium">Balogun Abiodun Abdullateef</td>
                                        </tr>
                                        <tr>
                                            <td>Class:</td>
                                            <td class="font-medium text-dark-medium">Basic 6</td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td class="font-medium text-dark-medium">Male</td>
                                        </tr>
                                        <tr>
                                            <td>Date Of Birth:</td>
                                            <td class="font-medium text-dark-medium">17<sup>th</sup> May, 2019</td>
                                        </tr>
                                        <tr>
                                            <td>Blood Group:</td>
                                            <td class="font-medium text-dark-medium">A</td>
                                        </tr>
                                        <tr>
                                            <td>Religion:</td>
                                            <td class="font-medium text-dark-medium">Islam</td>
                                        </tr>
                                        <tr>
                                            <td>Entry Date:</td>
                                            <td class="font-medium text-dark-medium">17<sup>th</sup> December, 2019</td>
                                        </tr>
                                        <tr>
                                            <td>ID No:</td>
                                            <td class="font-medium text-dark-medium">10005</td>
                                        </tr>
                                        <tr>
                                            <td>Class Teacher:</td>
                                            <td class="font-medium text-dark-medium">Mrs Risi Abiodun</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection