@extends('layouts.backend')
@section('title', 'Account Settings')
@section('content')
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <ul>
            <li>
                <a href="{{ route('dashboard.index')}}">Dashboard</a>
            </li>
            <li>Account Settings</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Password Update Area Start Here -->
    <div class="card height-auto">
        <div class="card-body">
            <div class="heading-layout1">
                <div class="item-title mg-b-20 mg-t-20">
                    <h3>Profile Information</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <h2>Account</h2>
                    <p class="subtitle">Update your account information, such as email and name.</p>
                </div>
                <div class="col-lg-9">
                    <form class="new-added-form">
                        <div class="row">
                            <div class="col-md-4 col-lg-4  col-sm-12 col-12 form-group">
                                <label>First Name</label>
                                <input type="text" placeholder="first name" class="form-control">
                            </div>
                            <div class="col-md-4 col-lg-4  col-sm-12 col-12 form-group">
                                <label>Last Name</label>
                                <input type="text" placeholder="last name" class="form-control">
                            </div>

                            <div class="col-md-4 col-lg-4  col-sm-12 col-12 form-group">
                                <label>Phone Number</label>
                                <input type="text" placeholder="phone number" class="form-control">
                            </div>

                            <div class="col-md-4 col-lg-5  col-sm-12 col-12 form-group">
                                <label>Email Address</label>
                                <input type="text" placeholder="valid email address" class="form-control">
                            </div>

                            <div class="col-md-4 col-lg-7  col-sm-12 col-12 form-group">
                                <label>Contact Address</label>
                                <input type="text" placeholder="valid address" class="form-control">
                            </div>


                            <div class="col-12 form-group" style="margin-top:36px;">
                                <button type="submit"
                                    class="btn-fill-lg shadow-violet-blue bg-mauvelous  btn-hover-bluedark">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <!-- Password Update  Area End Here -->

    <!-- Password Update Area Start Here -->
    <div class="card height-auto">
        <div class="card-body">
            <div class="heading-layout1">
                <div class="item-title mg-b-20 mg-t-20">
                    <h3>Password Settings</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <h2>Password</h2>
                    <p class="subtitle">Change your password.</p>
                </div>
                <div class="col-lg-9">
                    <form class="new-added-form">
                        <div class="row">
                            <div class="col-md-4 col-lg-4  col-sm-12 col-12 form-group">
                                <label>New Password</label>
                                <input type="password" placeholder="" class="form-control">
                            </div>
                            <div class="col-md-4 col-lg-4  col-sm-12 col-12 form-group">
                                <label>Repeat New Password</label>
                                <input type="password" placeholder="************" class="form-control">
                            </div>

                            <div class="col-md-4 col-lg-4  col-sm-12 col-12 form-group" style="margin-top:36px;">
                                <button type="submit"
                                    class="btn-fill-lg shadow-violet-blue bg-mauvelous  btn-hover-bluedark">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <!-- Password Update  Area End Here -->
    @endsection