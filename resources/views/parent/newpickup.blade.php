@extends('layouts.backend')
@section('title', 'Registered Pickup Assistants')
@section('content')
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <ul>
            <li>
                <a href="{{ route('dashboard.index')}}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('pickup.index')}}">Registered Pickup Assistant</a>
            </li>
            <li>New Pickup Assistant</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Add New Pickup Assistant Area Start Here -->
    <div class="card height-auto">
        <div class="card-body">
            <div class="heading-layout1">
                <div class="item-title mg-b-20 mg-t-20">
                    <h3>Add Pickup Assistant</h3>
                </div>
            </div>
            <form class="new-added-form">
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>First Name *</label>
                        <input type="text" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Last Name *</label>
                        <input type="text" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Gender *</label>
                        <select class="select2">
                            <option value="">Please Select Gender *</option>
                            <option value="1">Male</option>
                            <option value="2">Female</option>
                            <option value="3">Others</option>
                        </select>
                    </div>

                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Phone</label>
                        <input type="text" placeholder="" class="form-control">
                    </div>

                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>AUTH No</label>
                        <input type="text" placeholder="" class="form-control">
                    </div>

                    <div class="col-xl-8 col-lg-6 col-12 form-group">
                        <label>Address</label>
                        <input type="text" placeholder="" class="form-control">
                    </div>


                    <div class="col-lg-6 col-12 form-group mg-t-30">
                        <label class="text-dark-medium">Upload Student Photo (150px X 150px)</label>
                        <input type="file" class="form-control-file">
                    </div>
                    <div class="col-12 form-group mg-t-8">
                        <button type="submit"
                            class="btn-fill-lg shadow-violet-blue bg-mauvelous  btn-hover-bluedark">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Add New Pickup Assistant Area End Here -->
    @endsection