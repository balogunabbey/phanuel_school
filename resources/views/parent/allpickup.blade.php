@extends('layouts.backend')
@section('title', 'Registered Pickup Assistants')
@section('content')
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <ul>
            <li>
                <a href="{{ route('dashboard.index')}}">Dashboard</a>
            </li>
            <li>Registered Pickup Assistant</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <div class="row">
        <div class="col-5-xxxl col-12">
            <div class="card dashboard-card-twelve">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title mg-t-20 mg-b-20">
                            <h3>Pickup Assistant</h3>
                        </div>
                    </div>
                    <div class="kids-details-wrap">
                        <div class="row">
                            <div class="col-12-xxxl col-xl-6 col-12">
                                <div class="kids-details-box mb-5">
                                    <div class="item-img" style="border-radius: 0%;">
                                        <img src="http://eportal.test/static/auth/img/figure/student.png" alt="kids">
                                    </div>
                                    <div class="item-content table-responsive">
                                        <table class="table text-nowrap">
                                            <tbody>
                                                <tr>
                                                    <td>Full Name:</td>
                                                    <td>Quadri Allan Balogun</td>
                                                </tr>
                                                <tr>
                                                    <td>Gender:</td>
                                                    <td>Female</td>
                                                </tr>
                                                <tr>
                                                    <td>Entry ID:</td>
                                                    <td>PH-0021</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone Number:</td>
                                                    <td>08062832467</td>
                                                </tr>
                                                <tr>
                                                    <td>Security Question:</td>
                                                    <td>Whats your best color?</td>
                                                </tr>
                                                <tr>
                                                    <td>Answer:</td>
                                                    <td>Pink</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12-xxxl col-xl-6 col-12">
                                <div class="kids-details-box">
                                    <div class="item-img" style="border-radius: 0%;">
                                        <img src="http://eportal.test/static/auth/img/figure/student.png" alt="kids">
                                    </div>
                                    <div class="item-content table-responsive">
                                        <table class="table text-nowrap">
                                            <tbody>
                                                <tr>
                                                    <td>Name:</td>
                                                    <td>Balogun Abiodun</td>
                                                </tr>
                                                <tr>
                                                    <td>Gender:</td>
                                                    <td>Male</td>
                                                </tr>
                                                <tr>
                                                    <td>Entry ID:</td>
                                                    <td>PH-0021</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone Number:</td>
                                                    <td>08062832467</td>
                                                </tr>

                                                <tr>
                                                    <td>Security Question:</td>
                                                    <td>Whats your best food?</td>
                                                </tr>
                                                <tr>
                                                    <td>Answer:</td>
                                                    <td>Amala</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection