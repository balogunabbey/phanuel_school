@extends('layouts.backend')
@section('title', 'Result Summary')
@section('content')
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <ul>
            <li>
                <a href="{{ route('dashboard.index')}}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('grade.index')}}">Search Result</a>
            </li>
            <li>Result Summary</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Student Attendence Search Area Start Here -->
    <div class="card height-auto">
        <div class="card-body">
            <style>
                #DataTables_Table_0_paginate,
                #DataTables_Table_1_paginate {
                    display: none;
                }
            </style>

            <div class="row">

                <div class="col-sm-6 col-lg-4 mg-t-20">
                    <label class="pupil-info">Pupil Information</label>
                    <ul class="list-unstyled bd-sm-l" style="line-height: 1.7;">
                        <li class="d-flex justify-content-between sub-pupil-info">
                            <span>Full Name</span>
                            <strong>Balogun Abiodun</strong>
                        </li>
                        <li class="d-flex justify-content-between sub-pupil-info">
                            <span>Pupil Class</span>
                            <strong>Grade 2</strong>
                        </li>
                        <li class="d-flex justify-content-between sub-pupil-info">
                            <span>Pupil Session</span>
                            <strong>2018 / 2019</strong>
                        </li>
                        <li class="d-flex justify-content-between sub-pupil-info">
                            <span>Pupil Term</span>
                            <strong>Second</strong>
                        </li>
                    </ul>
                </div><!-- col -->
                <div class="col-sm-6 col-lg-4 mg-t-20">
                    <label class="pupil-info">Other Information</label>
                    <ul class="list-unstyled bd-sm-l" style="line-height: 1.7; padding: 10px 30px 0px 0px;">
                        <li class="d-flex justify-content-between sub-pupil-info">
                            <span>Pupil Gender</span>
                            <strong>Female</strong>
                        </li>
                        <li class="d-flex justify-content-between sub-pupil-info">
                            <span>Best Subject</span>
                            <strong>Mathematics</strong>
                        </li>
                        <li class="d-flex justify-content-between sub-pupil-info">
                            <span>Least Subject</span>
                            <strong>English Language</strong>
                        </li>
                        <li class="d-flex justify-content-between sub-pupil-info">
                            <span>Class Ranking</span>
                            <strong>1 out 25</strong>
                        </li>
                    </ul>
                </div><!-- col -->

                <div class="col-sm-6 col-lg-4 mg-t-20">
                    <label class="pupil-info">Academic Information</label>
                    <ul class="list-unstyled" style="line-height: 1.7; padding: 10px 0px 0px 0px;">
                        <li class="d-flex justify-content-between sub-pupil-info">
                            <span>Class Performance</span>
                            <strong>74.8%</strong>
                        </li>
                        <li class="d-flex justify-content-between sub-pupil-info">
                            <span>Termly Performance</span>
                            <strong>10.43% <i class="fas fa-long-arrow-alt-up text-dark-pastel-green"></i></strong>
                        </li>
                        <li class="d-flex justify-content-between sub-pupil-info">
                            <span>Session Performance</span>
                            <strong>20.43% <i class="fas fa-long-arrow-alt-down text-orange-peel"></i></strong>
                        </li>
                        <li class="d-flex justify-content-between sub-pupil-info">
                            <span>Total Attendance</span>
                            <strong>209 / 300</strong>
                        </li>
                    </ul>
                </div><!-- col -->
            </div>

            <!-- Class Routine Area Start Here -->
            <div class="row">
                <div class="col-8-xxxl col-8 col-12  mg-t-30">
                    <div class="table-responsive">
                        <table class="table display data-table text-nowrap bg-true-v">
                            <thead>
                                <tr>
                                    <th width="50%">Subject</th>
                                    <th width="16%" class="text-center">C.A</th>
                                    <th width="18%" class="text-center">Exam Score</th>
                                    <th width="16%" class="text-center">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Mathematics</td>
                                    <td class="text-center">25</td>
                                    <td class="text-center">39</td>
                                    <td class="text-center">61</td>
                                </tr>

                                <tr>
                                    <td>English Language</td>
                                    <td class="text-center">25</td>
                                    <td class="text-center">39</td>
                                    <td class="text-center">61</td>
                                </tr>
                                <tr>
                                    <td>Computer</td>
                                    <td class="text-center">25</td>
                                    <td class="text-center">39</td>
                                    <td class="text-center">61</td>
                                </tr>

                                <tr>
                                    <td>Argricultural Science</td>
                                    <td class="text-center">25</td>
                                    <td class="text-center">39</td>
                                    <td class="text-center">61</td>
                                </tr>

                                <tr>
                                    <td>Creative Arts</td>
                                    <td class="text-center">25</td>
                                    <td class="text-center">39</td>
                                    <td class="text-center">61</td>
                                </tr>

                                <tr>
                                    <td>Vocational Aptitude</td>
                                    <td class="text-center">25</td>
                                    <td class="text-center">39</td>
                                    <td class="text-center">61</td>
                                </tr>


                                <tr>
                                    <td>Social Studies</td>
                                    <td class="text-center">25</td>
                                    <td class="text-center">39</td>
                                    <td class="text-center">61</td>
                                </tr>

                                <tr>
                                    <td>PH Education</td>
                                    <td class="text-center">25</td>
                                    <td class="text-center">39</td>
                                    <td class="text-center">61</td>
                                </tr>

                            </tbody>
                        </table>

                        <div class="row justify-content-between">
                            <div class="col-sm-6 col-lg-6 ">
                                <label class="teacher-remark mg-t-20">Teacher's Remark</label>
                                <p style="font-size:15px; font-weight:400;">Sed ut perspiciatis unde omnis iste natus
                                    error sit
                                    voluptatem accusantium doloremque.</p>
                            </div><!-- col -->
                            <div class="col-sm-6 col-lg-4 mg-t-20">
                                <table class="table display data-table text-nowrap bg-true-v">
                                    <thead>
                                        <tr>
                                            <th width="50%">Summary</th>
                                            <th width="50%" class="text-center">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <td>Cummulative</td>
                                            <td class="text-center">25</td>
                                        </tr>

                                        <tr>
                                            <td>Grade Total</td>
                                            <td class="text-center">25</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div><!-- col -->
                        </div>
                    </div>
                </div>
                {{-- <div class="col-8-xxxl col-4  col-12  mg-t-30">
                    <div class="table-responsive">
                        <table class="table data-table text-nowrap bg-true-v">
                            <thead>
                                <tr>
                                    <th width="100%" style="color: #fff;">Summary</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Grand Total</td>
                                    <td class="text-center">25</td>
                                </tr>

                                <tr>
                                    <td>Grade</td>
                                    <td class="text-center">25</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="remark mg-t-30">
                        <h6>Teacher's Remark</h6>
                        <p> He is a good soul and happy child</p>
                    </div>
                </div> --}}
            </div>
            <!-- Class Routine Area End Here -->



        </div>
    </div>
    <!-- Student Attendence Search Area End Here -->
    @endsection