@extends('layouts.backend')
@section('title', 'Exemption Request')
@section('content')
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <ul>
            <li>
                <a href="{{ route('dashboard.index')}}">Dashboard</a>
            </li>
            <li>School Absence Request</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- All Requests Area Start Here -->
    <div class="row">
        <div class="col-4-xxxl col-12">
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>Create New Request</h3>
                        </div>
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="all-subject.html#" role="button" data-toggle="dropdown"
                                aria-expanded="false">...</a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="fas fa-plus text-dark-pastel-green"></i>Add
                                    Another Request</a>
                            </div>
                        </div>
                    </div>
                    <form class="new-added-form">
                        <div class="row">

                            <div class="col-12-xxxl col-lg-3 col-12 form-group">
                                <label>Starting From *</label>
                                <input type="text" placeholder="dd/mm/yyyy" class="form-control air-datepicker"
                                    data-position='bottom right'>
                                <i class="far fa-calendar-alt"></i>
                            </div>

                            <div class="col-12-xxxl col-lg-3 col-12 form-group">
                                <label>Ending by *</label>
                                <input type="text" placeholder="dd/mm/yyyy" class="form-control air-datepicker"
                                    data-position='bottom right'>
                                <i class="far fa-calendar-alt"></i>
                            </div>
                            <div class="col-12-xxxl col-lg-6 col-12 form-group">
                                <label>Affected Kid(s) <span style="font-size:12px;" class="text-red">(Multiple
                                        Selection is allowed)*</span></label>
                                <select class="select2" multiple>
                                    <option value="">Please Select</option>
                                    <option value="1">Balogun Abiodun Lateef</option>
                                    <option value="2">Kingsley Abayomi Lateef</option>
                                </select>
                            </div>

                            <div class="col-12-xxxl col-lg-12 col-12 form-group">
                                <label>Possible Reasons *</label>
                                <textarea class="textarea form-control" name="message" id="form-message" cols="10"
                                    rows="4"></textarea>
                            </div>

                            <div class="col-12 form-group mg-t-8">
                                <button type="submit"
                                    class="btn-fill-lg text-light shadow-violet-blue bg-mauvelous btn-hover-bluedark">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-8-xxxl col-12">
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title mg-b-20  mg-t-20">
                            <h3>All Exemption Requests</h3>
                        </div>
                    </div>
                    <form class="mg-b-20">
                        <div class="row gutters-8">
                            <div class="col-lg-4 col-12 form-group">
                                <input type="text" placeholder="Starting from" class="form-control air-datepicker"
                                    data-position='bottom right'>
                            </div>
                            <div class="col-lg-4 col-12 form-group">
                                <input type="text" placeholder="Ending by" class="form-control air-datepicker"
                                    data-position='bottom right'>
                            </div>
                            <div class="col-lg-2 col-12 form-group">
                                <button type="submit"
                                    class="fw-btn-fill shadow-orange-peel btn-gradient-yellow">SEARCH</button>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table display data-table text-nowrap bg-true-v">
                            <thead>
                                <tr>
                                    <th>Request ID</th>
                                    <th>Affected Kid(s)</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>#99281</td>
                                    <td>Balogun Abiodun</td>
                                    <td>02/05/2001</td>
                                    <td>02/05/2001</td>
                                    <td class="badge badge-pill badge-danger shadow-red mg-t-8">Pending</td>
                                    <td>
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#"><i
                                                        class="fas fa-trash text-orange-red"></i>Delete</a>
                                                <a class="dropdown-item" href="#"><i
                                                        class="fas fa-eye text-dark-pastel-green"></i>View</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#99285</td>
                                    <td>Balogun Abdullateef</td>
                                    <td>02/05/2001</td>
                                    <td>02/05/2001</td>
                                    <td class="badge badge-pill badge-success shadow-dark-pastel-green mg-t-8">Approved
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <a href="all-subject.html#" class="dropdown-toggle" data-toggle="dropdown"
                                                aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#"><i
                                                        class="fas fa-trash text-orange-red"></i>Delete</a>
                                                <a class="dropdown-item" href="#"><i
                                                        class="fas fa-eye text-dark-pastel-green"></i>View</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- All Requests Area End Here -->


    @endsection