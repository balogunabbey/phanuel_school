@extends('layouts.backend')
@section('title', 'Result Checker')
@section('content')
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <ul>
            <li>
                <a href="{{ route('dashboard.index')}}">Dashboard</a>
            </li>
            <li>School Tuition</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Tuitions Area Start Here -->
    <div class="row">
        <div class="col-8-xxxl col-12">
            <!-- Expanse Table Area Start Here -->
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title mg-t-20 mg-b-20">
                            <h3>School Fees History</h3>
                        </div>
                    </div>
                    <form class="mg-b-20">
                        <div class="row gutters-8">
                            <div class="col-4-xxxl col-xl-3 col-lg-4 col-12 form-group">
                                <input type="text" placeholder="Search by Session ..." class="form-control">
                            </div>
                            <div class="col-4-xxxl col-xl-3 col-lg-4 col-12 form-group">
                                <input type="text" placeholder="Search by Term" class="form-control">
                            </div>
                            <div class="col-4-xxxl col-xl-4 col-lg-4 col-12 form-group">
                                <input type="text" placeholder="Search by Name" class="form-control">
                            </div>
                            <div class="col-1-xxxl col-xl-2 col-lg-4 col-12 form-group">
                                <button type="submit"
                                    class="fw-btn-fill btn-gradient-yellow shadow-orange-peel">SEARCH</button>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table data-table text-nowrap bg-true-v">
                            <thead>
                                <tr>
                                    <th>Session</th>
                                    <th>Term</th>
                                    <th>Class</th>
                                    <th>Name</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Deadline</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2016/2017</td>
                                    <td>Second Contact</td>
                                    <td>Basic 4</td>
                                    <td>Balogun Abiodun</td>
                                    <td>$2,0000.00</td>
                                    <td class="badge badge-pill badge-success d-block mg-t-8  shadow-dark-pastel-green">
                                        Paid</td>
                                    <td>02/02/2019</td>
                                    <td>
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#"><i
                                                        class="fas fa-eye text-orange-red"></i>View Invoice</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2016/2017</td>
                                    <td>Second Contact</td>
                                    <td>Basic 4</td>
                                    <td>Balogun Abiodun</td>
                                    <td>$2,0000.00</td>
                                    <td class="badge badge-pill badge-danger d-block mg-t-8 shadow-orange-red">pending
                                    </td>
                                    <td>02/02/2019</td>
                                    <td>
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#"><i
                                                        class="fas fa-eye text-dark-pastel-green"></i>View
                                                    Invoice</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Expanse Table Area End Here -->
        </div>
    </div>
    <!-- Tuitions Area End Here -->
    @endsection