@extends('layouts.app')
@section('title', 'News &amp; Events')
@section('content')
<!-- Inner Banner -->
<div class="inner-banner team text-center" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-03.jpg') }}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>Keep up to date with all the latest news happening in school at the moment.</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->

<!-- Main -->
<main id="main">

    <!-- Blogs -->
    <section class="blogs-holder tc-padding-bottom gray-bg">
        <div class="container">
            <div class="row">

                <!-- Content -->
                <div class="col-lg-12 col-md-12 col-xs-12">

                    <!-- Classes Content -->
                    <div class="content has-layout">

                        <!-- Breadcrumbs -->
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{{ route('about.page') }}"><i class="icon-folder"></i> About Us</a></li>
                                <li> News and Events</li>
                                <li><a href="{{ route('home.page') }}"><i class="icon-home22"></i> Back to Home</a></li>
                            </ul>
                        </div>
                        <!-- Breadcrumbs -->

                        <!-- Blogs List -->
                        <div class="blogs-list-holder">


                            <!-- Img Post -->
                            <div class="post-widget">
                                <h2>How to clean your teeth</h2>
                                <div class="post-detail">
                                    <div class="meta-post">
                                        <ul>
                                            <li><i class="icon-calendar"></i>12, May 2019</li>
                                            <li><i class="icon-user"></i>Administrator</li>
                                        </ul>
                                    </div>
                                    <p>The aim of the school is to provide a broad and balanced curriculum which will
                                        enable children to develop intellectually, aesthetically, physically, socially,
                                        morally and spiritually.</p>
                                    <a class="continue-read position-center-y"
                                        href="{{ route('newspreview.page')}}">Continue Reading</a>
                                </div>
                            </div>
                            <!-- Img Post -->




                        </div>
                        <!-- Blogs List -->


                    </div>
                    <!-- Classes Content -->

                </div>
                <!-- Content -->


            </div>
        </div>
    </section>
    <!-- Blogs -->

</main>
<!-- Main -->
@endsection