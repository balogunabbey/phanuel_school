@extends('layouts.app')
@section('title', 'About Us')
@section('content')
<!-- Inner Banner -->
<div class="inner-banner gallery text-center" data-enllax-ratio="-.3"
    style="background: url({{ asset('static/images/inner-banners/img-02.jpg') }}) 50% 0% no-repeat fixed;">
    <div class="container">
        <div class="inner-heading">
            <h2>What you need to know<br> about us</h2>
        </div>
    </div>
</div>
<!-- Inner Banner -->
<!-- Main -->
<main id="main" class="about-bg">

    <!-- About Holder -->
    <section class="about-holder tc-padding">
        <div class="container">
            <div class="about-content has-layout">

                <!-- About Nersery -->
                <div class="row">

                    <!-- About Img -->
                    <div class="col-sm-6">
                        <div class="about-img-2"><img src="{{ asset('static/images/welcome.gif') }}" alt=""></div>
                    </div>
                    <!-- About Img -->

                    <!-- About Text -->
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="about-text style-2">
                            <h3>Who We Are?</h3>
                            <h4> Register your wards today at PHANUEL SCHOOLS where early development is fundamental to
                                the growth of your child.</h4>
                            <p style="text-align:justify;"></p>

                            <div class="kids-img has-layout">
                                <ul>
                                    <li class="animate swing" data-wow-delay="0.2s"><img
                                            src="{{ asset('static/images/kids-imgs/img-01.png') }}" alt=""></li>
                                    <li class="animate lightSpeedIn" data-wow-delay="0.4s"><img
                                            src="{{ asset('static/images/kids-imgs/img-02.png') }}" alt=""></li>
                                    <li class="animate swing" data-wow-delay="0.4s"><img
                                            src="{{ asset('static/images/kids-imgs/img-03.png') }}" alt=""></li>
                                    <li class="animate lightSpeedIn" data-wow-delay="0.8s"><img
                                            src="{{ asset('static/images/kids-imgs/img-04.png') }}" alt=""></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- About Text -->

                </div>
                <!-- About Nersery -->

                <!-- Our Mission -->
                <div class="our-mission-holder">
                    <div class="row">

                        <!-- About Text -->
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="about-text has-layout">
                                <h3 style="font-size: 43px;">Visions and Values</h3>
                                <h4>Our visions and values are at the core of everything we do. They underpin our
                                    teaching and learning, and provide an environment which prepares our pupils as
                                    confident, happy citizens.</h4>
                                <p>We understand that choosing the right school for your child is an important decision
                                    in any family's life. Here at Phanuel Schools, we make every effort to produce a
                                    happy atmosphere with mutual respect shown by everyone in our school family. We aim
                                    to create an environment where each child is made to feel secure, wanted and valued.
                                </p>
                                <p>At Phanuel Schools, our dedicated and hardworking staff aim to provide children with
                                    a broad and balanced curriculum. We strive to enable every child, regardless of
                                    ability, ethnicity, religion or disability to develop their full potential, both
                                    educationally and socially, so that they are fulfilled and happy within our caring
                                    school. We know that this will only be achieved through partnership with the
                                    parents.</p>

                            </div>
                        </div>
                        <!-- About Text -->

                        <!-- About Img -->
                        <div class="col-sm-6">
                            <div class="our-mission"><img src="{{ asset('static/images/our-mission.png') }}"
                                    alt="Our Mission"></div>
                        </div>
                        <!-- About Img -->

                    </div>
                </div>
                <!-- Our Mission -->


                <!-- School -->
                <div class="session-news curve-down style-2 has-layout">
                    <div class="enroll-now">
                        <p>Register your child today at PHANUEL SCHOOLS, a great place to learn through play.</p>
                        <a class="tc-btn" href="{{ route('admission.enrol')}}">Enroll Now</a>
                    </div>
                </div>
                <!-- School -->

            </div>
        </div>
    </section>
    <!-- About Holder -->




</main>
<!-- Main -->
@endsection